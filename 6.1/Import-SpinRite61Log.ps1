#Requires -Version 7
<#
.SYNOPSIS
This script reads SpinRite 6.1 log files and parses the values out of them.

.NOTES
Version 147

If you've never run PowerShell before, here's a quick guide:
https://blog.udemy.com/run-powershell-script/

This script requires PowerShell 7 or later.
https://github.com/PowerShell/PowerShell/releases
winget install Microsoft.PowerShell

This script lives here:
https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Import-SpinRite61Log.ps1

Use this link to download it:
https://gitlab.com/GRC-Community/SpinRite-Logs/-/raw/main/6.1/Import-SpinRite61Log.ps1?inline=false

.EXAMPLE
$Logs = .\Import-SpinRite61Log.ps1 -LogPath 'Path\To\Your\Logs' -Verbose

Processes your logs, outputs status messages, and saves the result in $Logs.

This script can take some time to run if you have a lot of logs, and/or large logs. That's why I recommend
saving the output into a variable.

.EXAMPLE
$Logs | Out-GridView

Displays the results in a spreadsheet-like window. Fair warning: it's laggy.

.EXAMPLE
$Logs | Export-Csv logs.csv

Save the results to a CSV file.

.EXAMPLE
$Logs | Format-Table -AutoSize Owner, Log, Model, 'serial number', Level, 'Mini Benchmark Speed', 'Average Speed', 'Completed Time', 'Temperature Celsius'

View just a few columns within PowerShell.

The full list of available properties is in the New-OutputHashtable function below.
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory = $true)]
    # The path to your SRLOGS folder.
    [String]$LogPath,

    # The number of CPU threads you want this script to use.
    [Int32]$Threads = [Math]::Min([Environment]::ProcessorCount, 6),

    [String]$DateFormat = 'yyyy-MM-dd HH:mm:ss',

    # Skip processing logs greater than this size (in bytes) to speed up execution.
    # You can use shortcuts like 25kb and 4mb. Do not put them in quotes!
    [Int32]$SkipSize,

    # Retrieve logs from multiple subdirectories.
    [Switch]$Recurse,

    # Write a Verbose line at the start of processing for every file.
    [Switch]$LogEveryFile
)
    
$StopWatch = [System.Diagnostics.Stopwatch]::StartNew()

$LogFilesParams = @{
    'Path'    = $LogPath
    'Filter'  = '*.log'
    'Recurse' = $Recurse
}
$LogFiles = Get-ChildItem @LogFilesParams | Where-Object Length -gt 0

if ( $SkipSize ) {

    $TotalLogCount = $LogFiles.Count
    $LogFiles = $LogFiles | Where-Object Length -lt $SkipSize
    'Skipped {0:N0} logs' -f ($TotalLogCount - $LogFiles.Count) | Write-Verbose
    
}

Write-Verbose "$($LogFiles.Count) logs to process."
Write-Verbose "Threads: $Threads"

#region Slicing
$SlicingStart = $StopWatch.Elapsed.TotalMilliseconds
# Chop the list of files into slices for efficient parallel processing.
# Attempt to spread files evenly among slices by file size.
$SlicedLogs = @{}
for ( $ThreadCount = 0; $ThreadCount -lt $Threads; $ThreadCount ++ ) {
    
    $ThreadTable = @{
        Logs        = New-Object System.Collections.Generic.List[Array]
        TotalLength = 0
    }
    $SlicedLogs.Add($ThreadCount, $ThreadTable)

}

$LogFiles | Sort-Object Length -Descending | ForEach-Object {

    $SmallestSliceSize = [Int64]::MaxValue
    $SmallestSliceKey = $null
    foreach ( $Slice in $SlicedLogs.GetEnumerator() ) {

        if ( $Slice.Value['TotalLength'] -lt $SmallestSliceSize ) {

            $SmallestSliceKey = $Slice.Key
            $SmallestSliceSize = $Slice.Value['TotalLength']

        }

    }
    
    ($SlicedLogs[$SmallestSliceKey]['Logs']).Add($_)
    $SlicedLogs[$SmallestSliceKey]['TotalLength'] += $_.Length

}
'Slicing took: {0:N1} ms' -f ($StopWatch.Elapsed.TotalMilliseconds - $SlicingStart) | Write-Verbose

$SlicedLogs.GetEnumerator() | ForEach-Object -ThrottleLimit $Threads -Parallel {

    $ScanStopwatch = [System.Diagnostics.Stopwatch]::StartNew()

    # I want the output object to always contain the same properties, so I start with a template hashtable.
    # I want the properties to always be in the same order, so I use an Ordered Dictionary.
    # However, Ordered Dictionaries cannot be cloned, so this is my workaround.
    #region Hashtable
    function New-OutputHashtable {

        [CmdletBinding()]
        param (
            $Owner,
            $Log,
            $OldTable = @{}
        )

        $OwnerUrl = [Uri]::EscapeDataString($Owner)
        $LogLink = "https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/$OwnerUrl/$($Log.Name)"

        try {

            # Add leading zeroes if the name is numeric. This helps with sorting.
            $LogName = '{0:D4}' -f [Int32]$Log.BaseName

        } catch {

            $LogName = $Log.BaseName

        }

        [Ordered]@{
            'Owner'                              = $Owner # The folder this log resides in.
            'Log'                                = $LogName # File name of the current log, without the extension.
            'Log Link'                           = $LogLink
            'Log Link MD'                        = "[$($Log.BaseName)]($LogLink)" # Log Link, formatted in Markdown.
            'Scan'                               = $null
        
            # Fields from the top of the log.
            'Version'                            = $OldTable['Version'] # The version of SpinRite that created the log. Added in Alpha 9.
            'Release Type'                       = $OldTable['Release Type'] # Whatever random thing Steve decided to call this build :)
            'Equivalent Version'                 = $OldTable['Equivalent Version'] # An approximate version number for special releases.
            'Command Line'                       = $OldTable['Command Line'] # Added in Alpha 28.
            'Boot Drive Port'                    = $OldTable['Boot Drive Port']
            'Boot Drive Size'                    = $OldTable['Boot Drive Size']
            'Boot Drive Size Bytes'              = $OldTable['Boot Drive Size Bytes']
            'Level'                              = $null
            'Start Time'                         = $null
            'End Time'                           = $null
            'Stop Type'                          = $null # Shows whether the scan completed, or was interrupted.
            'From %'                             = $null # Added in Alpha 10.
            'From Sector'                        = $null # Added in Alpha 10.
            'To %'                               = $null # Added in Alpha 10.
            'To Sector'                          = $null # Added in Alpha 10.
            'Sector Count Difference'            = $null # The difference between 'To Sector' and 'sector count'. See:
            # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/428
            'Model'                              = $null
            'Runtime'                            = $null # Equivalent to Power_On_Hours in smartctl. Added in Alpha 9.

            # These lowercase fields are from the drive info section of the log, right under the model number.
            # I kept them exactly as they appear in the logs to make it easier to process them.
            'serial number'                      = $null
            'access mode'                        = $null
            'sector bytes'                       = $null
            'pci bus addr'                       = $null
            'firmware rev'                       = $null
            'adapter vendor'                     = $null
            'rotation rate'                      = $null
            'block transfer'                     = $null
            'vendor-device'                      = $null
            'ata/atapi spec'                     = $null
            'bios drive'                         = $null
            'drv technology'                     = $null
            'attached port'                      = $null
            'controller reg'                     = $null
            'bus master reg'                     = $null
            'lba in use'                         = $null
            'port speed'                         = $null
            'max driv speed'                     = $null
            'multi-word dma'                     = $null
            'ultradma modes'                     = $null
            'sector count'                       = $null
            'byte count'                         = $null
            'available pio'                      = $null
            '4K Sectors'                         = $null # Added in RC 5.01.
            'features'                           = $null
            'feature detail'                     = $null
            'transfers'                          = $null
            'long ops'                           = $null
            'cyls/hds/sects'                     = $null
            'phys cylinders'                     = $null
            'physical heads'                     = $null
            'physical sects'                     = $null
            'Mini Benchmark Speed'               = $null # The name of the "speed" field changes, so I rename it below.
            'BIOS Bugs'                          = $null # Added in RC 5.07.
        
            # Performance Benchmarks - Before
            'Before SMART Delay'                 = $null
            'Before Random Sectors'              = $null
            'Before Front'                       = $null
            'Before Midpoint'                    = $null
            'Before End'                         = $null

            # Events
            'Errors'                             = @{}
            'Missing Events Divider'             = $false # Is the divider after Events missing?
            'Longest Reset'                      = $null # Added in RC 5.01.
            'Temperature Events'                 = $null # Added in RC 4

            # Performance Benchmarks - After
            'After SMART Delay'                  = $null # Added in Alpha 19.
            'After Random Sectors'               = $null # Added in Alpha 19.
            'After Front'                        = $null # Added in Alpha 19.
            'After Midpoint'                     = $null # Added in Alpha 19.
            'After End'                          = $null # Added in Alpha 19.

            'Manual Benchmark Count'             = 0

            # Graphic Status Display
            'Remaining MB'                       = $null
            'Completed MB'                       = $null
            'Location MB'                        = $null # Alpha 9 changed 'Completed MB' to 'Location MB'.
            'Remaining %'                        = $null
            'Completed %'                        = $null
            'Location %'                         = $null # Alpha 9 changed 'Completed %' to 'Location %'.
            'GSD Completed %'                    = $null # Completed %, based on the GSD blocks.
            'Completed Difference'               = $null # The difference between 'Completed %' and 'GSD Completed %'.
            'Block Resolution'                   = $null # The percentage of a scan that 1 GSD block represents.
            'GSD Block Count'                    = $null
            'Start %'                            = $null # Where the scan started, based on the GSD blocks.
            'End %'                              = $null # Where the scan finished, based on the GSD blocks.
            'Remaining Time'                     = $null
            'Completed Time'                     = $null
            'Scan Time'                          = $null # Completed Time, based on the difference between Start Time and End Time.
            'Scan Time Difference'               = $null # The difference in seconds between Completed Time and Scan Time, if > 60.
            'Average Speed'                      = $null # How fast the drive transferred data in MB/s, calculated below.
            'Average Speed Skipped'              = $null # The reason why Average Speed was skipped.

            # Final Sector Event Counts
            'Command Timeout'                    = $null
            'Command Aborted'                    = $null
            'Comm/Cable Errors'                  = $null
            'Not Recoverable'                    = $null
            'Minor Troubles'                     = $null
            'Sector Never Found'                 = $null
            'DynaStat Recovered'                 = $null
            'Defective Sector'                   = $null

            # End-of-Run SMART System Status
            'ECC Corrected Current'              = $null
            'ECC Corrected Max'                  = $null
            'ECC Corrected Diff'                 = $null
            'ECC Corrected Raw'                  = $null
            'Read Channel Margin Current'        = $null
            'Read Channel Margin Max'            = $null
            'Read Channel Margin Diff'           = $null
            'Read Channel Margin Raw'            = $null
            'Relocated Sectors Current'          = $null
            'Relocated Sectors Max'              = $null
            'Relocated Sectors Diff'             = $null
            'Relocated Sectors Raw'              = $null
            'Reallocation Events Current'        = $null
            'Reallocation Events Max'            = $null
            'Reallocation Events Diff'           = $null
            'Reallocation Events Raw'            = $null
            'Seek Errors Current'                = $null
            'Seek Errors Max'                    = $null
            'Seek Errors Diff'                   = $null
            'Seek Errors Raw'                    = $null
            'Recalibration Retries Current'      = $null
            'Recalibration Retries Max'          = $null
            'Recalibration Retries Diff'         = $null
            'Recalibration Retries Raw'          = $null
            'Cabling Errors Current'             = $null
            'Cabling Errors Max'                 = $null
            'Cabling Errors Diff'                = $null
            'Cabling Errors Raw'                 = $null
            'Uncorrectable Current'              = $null
            'Uncorrectable Max'                  = $null
            'Uncorrectable Diff'                 = $null
            'Uncorrectable Raw'                  = $null
            'Write Errors Current'               = $null
            'Write Errors Max'                   = $null
            'Write Errors Diff'                  = $null
            'Write Errors Raw'                   = $null
            'Command Timeout Current'            = $null # Added in SRPR-037.
            'Command Timeout Max'                = $null # Added in SRPR-037.
            'Command Timeout Diff'               = $null
            'Command Timeout Raw'                = $null # Added in SRPR-037.
            'Pending Sectors Current'            = $null # Added in SRPR-037.
            'Pending Sectors Max'                = $null # Added in SRPR-037.
            'Pending Sectors Diff'               = $null
            'Pending Sectors Raw'                = $null # Added in SRPR-037.
            'Read Retries Current'               = $null # Added in SRPR-037.
            'Read Retries Max'                   = $null # Added in SRPR-037.
            'Read Retries Diff'                  = $null
            'Read Retries Raw'                   = $null # Added in SRPR-037.
            'Total Writes Current'               = $null # Added in SRPR-037.
            'Total Writes Max'                   = $null # Added in SRPR-037.
            'Total Writes Diff'                  = $null
            'Total Writes Raw'                   = $null # Added in SRPR-037.
            'Write Failures Current'             = $null # Added in SRPR-037.
            'Write Failures Max'                 = $null # Added in SRPR-037.
            'Write Failures Diff'                = $null
            'Write Failures Raw'                 = $null # Added in SRPR-037.
            'Wear Leveling Current'              = $null # Added in SRPR-037.
            'Wear Leveling Max'                  = $null # Added in SRPR-037.
            'Wear Leveling Diff'                 = $null
            'Wear Leveling Raw'                  = $null # Added in SRPR-037.
            'Remaining Life Current'             = $null # Added in SRPR-037.
            'Remaining Life Max'                 = $null # Added in SRPR-037.
            'Remaining Life Diff'                = $null
            'Remaining Life Raw'                 = $null # Added in SRPR-037.
            'Reallocation Space Current'         = $null # Added in SRPR-037.
            'Reallocation Space Max'             = $null # Added in SRPR-037.
            'Reallocation Space Diff'            = $null
            'Reallocation Space Raw'             = $null # Added in SRPR-037.
            'Missing Slash'                      = $false # Is the "/" between the "current" and "max" values missing?

            'Temperature Celsius'                = $null
            'Temperature Fahrenheit'             = $null

            'ECC Corrected Rate Count'           = $null
            'ECC Corrected Rate Min'             = $null
            'ECC Corrected Rate Current'         = $null
            'ECC Corrected Rate Max'             = $null
            'Reallocation Events Rate Count'     = $null
            'Reallocation Events Rate Min'       = $null
            'Reallocation Events Rate Current'   = $null
            'Reallocation Events Rate Max'       = $null
            'Seek Errors Rate Count'             = $null
            'Seek Errors Rate Min'               = $null
            'Seek Errors Rate Current'           = $null
            'Seek Errors Rate Max'               = $null
            'Recalibration Retries Rate Count'   = $null
            'Recalibration Retries Rate Min'     = $null
            'Recalibration Retries Rate Current' = $null
            'Recalibration Retries Rate Max'     = $null
            'Cabling Errors Rate Count'          = $null
            'Cabling Errors Rate Min'            = $null
            'Cabling Errors Rate Current'        = $null
            'Cabling Errors Rate Max'            = $null
            'Uncorrectable Rate Count'           = $null
            'Uncorrectable Rate Min'             = $null
            'Uncorrectable Rate Current'         = $null
            'Uncorrectable Rate Max'             = $null
            'Write Errors Rate Count'            = $null
            'Write Errors Rate Min'              = $null
            'Write Errors Rate Current'          = $null
            'Write Errors Rate Max'              = $null

            'Scan Header BIOS Corruption'        = $false # Corruption that occasionally occurs on BIOS drives.
        }

    }

    # The [TimeSpan] cast doesn't work as expected if the hours field is greater than 23.
    function ConvertFrom-LargeTimeSpan {

        [CmdletBinding()]
        param (
            [String]$LargeTimeSpan
        )

        $TimeSpanSplit = $LargeTimeSpan -split ':'

        New-TimeSpan -Hours $TimeSpanSplit[0] -Minutes $TimeSpanSplit[1] -Seconds $TimeSpanSplit[2]

    }

    function Get-EquivalentVersion {

        # Maps Steve's random names to approximate version numbers.
        # I'm going by the "Date modified" of the files I have downloaded, so this may be inaccurate.
        [CmdletBinding()]
        param (
            [String]$Version,
            [String]$ReleaseType,
            $ShortPath
        )

        if ( $ReleaseType -eq 'SpinRite v6.1 - Release' ) {

            $EquivalentVersion = '6.1.{0}' -f ([Int32]$Version - 1)

        } else {

            #region Equivalent Version
            $EquivalentVersion = '6.1.0-'
            $EquivalentVersion += switch ($ReleaseType) {
                'alpha-release' {
                    switch ($Version) {
                        '24a' { 'a24.1'; break } # srpr-24a
                        default {
                            # Semantic versioning requires padding for comparisons to work correctly.
                            'a{0:D2}' -f [Int32]$Version
                        }
                    }
                    break
                }
                'CTRL+Enter Test' {
                    switch ($Version) {
                        '01' { 'rc3.1'; break } # srpr-ce1
                        '02' { 'rc3.2'; break } # srpr-ce2
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'Deferred Logging Test' {
                    switch ($Version) {
                        'dlt' { 'a09.1'; break } # srpr-dlt
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'diagnostic trace' {
                    switch ($Version) {
                        'dt1' { 'a22.6'; break } # srpr-dt1
                        'dt2' { 'a23.1'; break } # srpr-dt2
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'drive discovery experiment' {
                    switch ($Version) {
                        '1' { 'a22.5'; break } # srpr-dde
                        'FF0' { 'a32.2'; break } # srpr-ff0
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'drive enumerator test' {
                    switch ($Version) {
                        '001' { 'a24.8'; break } # srpr-det-001
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'incremental dev release' {
                    switch ($Version) {
                        '030' { 'a25.2'; break } # srpr-030
                        '031' { 'a25.3'; break } # srpr-031
                        '032' { 'a26.1'; break } # srpr-032
                        '033' { 'a28.1'; break } # srpr-033
                        '034' { 'a28.2'; break } # srpr-034
                        '035' { 'a28.3'; break } # srpr-035
                        '036' { 'a28.4'; break } # srpr-036
                        '037' { 'a28.5'; break } # srpr-037
                        '038' { 'a28.6'; break } # srpr-038
                        '040' { 'a30.2'; break } # srpr-040
                        '041' { 'a31.1'; break } # srpr-041
                        '042' { 'a31.2'; break } # srpr-042
                        '043' { 'a31.3'; break } # srpr-043
                        '044' { 'a32.1'; break } # srpr-044
                        '045' { 'a32.3'; break } # srpr-045
                        'MQ15' { 'rc1.1'; break } # sr-mq15
                        'R15' { 'rc1.2'; break } # sr-r15
                        '046' { 'rc1.3'; break } # srpr-046
                        'MQ16' { 'rc1.4'; break } # sr-mq16
                        'R16' { 'rc1.5'; break } # sr-r16
                        'R17' { 'rc1.6'; break } # sr-r17
                        'MQ17' { 'rc1.7'; break } # sr-mq17
                        'MQ18' { 'rc1.8'; break } # sr-mq18
                        'MQ19' { 'rc1.9'; break } # sr-mq19
                        '048' { 'rc1.10'; break } # srpr-048
                        'R18' { 'rc1.11'; break } # sr-r18
                        '049' { 'rc1.12'; break } # srpr-049
                        '050' { 'rc1.13'; break } # srpr-050
                        '051' { 'rc1.14'; break } # srpr-051
                        '052' { 'rc1.15'; break } # srpr-052
                        '053' { 'rc1.16'; break } # srpr-053
                        '054' { 'rc1.17'; break } # srpr-054
                        '055' { 'rc1.18'; break } # srpr-055
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'limited test release' {
                    switch ($Version) {
                        'mq1' { 'a13.1'; break } # srpr-mq1
                        '001' { 'a13.2'; break } # srpr-001
                        '002' { 'a13.3'; break } # srpr-002
                        'cb1' { 'a17.1'; break } # srpr-cb1
                        'mq2' { 'a17.2'; break } # srpr-mq2
                        '003' { 'a17.3'; break } # srpr-003
                        '004' { 'a17.4'; break } # srpr-004
                        '005' { 'a19.1'; break } # srpr-005
                        '006' { 'a19.2'; break } # srpr-006
                        '007' { 'a19.3'; break } # srpr-007
                        '008' { 'a19.4'; break } # srpr-008
                        '009' { 'a21.1'; break } # srpr-009
                        '010' { 'a22.1'; break } # srpr-010
                        '011' { 'a22.2'; break } # srpr-011
                        '012' { 'a22.3'; break } # srpr-012
                        '013' { 'a22.4'; break } # srpr-013
                        '014' { 'a23.2'; break } # srpr-014
                        '015' { 'a23.3'; break } # srpr-015
                        '016' { 'a23.4'; break } # srpr-016
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'nightly test release' {
                    switch ($Version) {
                        '017' { 'a23.5'; break } # srpr-017
                        '018' { 'a24.2'; break } # srpr-018
                        '019' { 'a24.3'; break } # srpr-019
                        '020' { 'a24.4'; break } # srpr-020
                        '021' { 'a24.5'; break } # srpr-021
                        '022' { 'a24.6'; break } # srpr-022
                        '023' { 'a24.7'; break } # srpr-023
                        '024' { 'a24.9'; break } # srpr-024
                        '025' { 'a24.10'; break } # srpr-025
                        '026' { 'a24.11'; break } # srpr-026
                        '027' { 'a24.12'; break } # srpr-027
                        '028' { 'a24.13'; break } # srpr-028
                        '029' { 'a24.14'; break } # srpr-029
                        '039' { 'a30.1'; break } # srpr-039
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'pre-release' {
                    "rc$Version"
                }
                'Release Candidate' {
                    "rc$Version"
                }
                'special test release' {
                    switch ($Version) {
                        'MY0' { 'a25.1'; break } # srpr-my0
                        default { Write-Error "$ShortPath - Invalid version: '$ReleaseType $Version'" }
                    }
                    break
                }
                'test release' {
                    switch ($Version) {
                        'CJ0' { 'rc5.02.1'; break } # srpr-cj0
                        'PB0' { 'rc5.02.2'; break } # srpr-pb0
                        'PH0' { 'rc5.03.1'; break } # srpr-ph0
                        'PH1' { 'rc5.03.2'; break } # srpr-ph1
                        'EXP' { 'rc5.03.3'; break } # srpr-exp
                        'EX1' { 'rc5.03.4'; break } # srpr-ex1
                        'PF0' { 'rc5.05.1'; break } # srpr-pf0
                        'MQ0' { 'rc5.07.1'; break } # srpr-mq0
                        'MQA' { 'rc5.08.1'; break } # srpr-mqa
                        'MQB' { 'rc5.08.2'; break } # srpr-mqb
                        'BC0' { 'rc5.08.3'; break } # srpr-bc0
                        '601' { 'rc6.01'; break } # srpr-601
                        '602' { 'rc6.02'; break } # srpr-602
                        '603' { 'rc6.03'; break } # srpr-603
                    }
                }
                default { Write-Error "$ShortPath - Unable to find a match for: '$ReleaseType $Version'" }
            }

        }

        [SemVer]$EquivalentVersion

    }

    #region LogReader
    class LogReader {

        [Int32]$Index
        [String[]]$LogLines
        [Int32]$MaxIndex

        # Constructor.
        LogReader (
            [System.IO.FileInfo]$LogFileInfo
        ) {

            $this.LogLines = (Get-Content -Raw -Path $LogFileInfo) -split "`r`n|`n"
            $this.MaxIndex = $this.LogLines.Count - 1

        }

        # Methods.

        [Bool] EndOfFile () {

            return $this.Index -ge $this.MaxIndex

        }

        # The only way to have optional parameters is to use overloading, but that either introduces overhead or
        # duplicate code, so I chose to always require a parameter, even if it's "0".
        [String] ReadLine (
            [Int32]$SkipCount
        ) {

            $this.Index += 1 + $SkipCount
            return $this.LogLines[$this.Index]

        }

    }

    # Retrieve the necessary parameters.
    $LogPath = $Using:LogPath
    $DateFormat = $Using:DateFormat
    $LogEveryFile = $Using:LogEveryFile
    $VerbosePreference = $Using:VerbosePreference

    

    :File foreach ( $LogFile in $_.Value['Logs'] ) {

        $ParentName = $LogFile.PSParentPath | Split-Path -Leaf
        $ShortPath = Join-Path $ParentName $LogFile.Name
        $ScanCount = 0

        if ( $LogEveryFile ) {

            Write-Verbose $ShortPath

        }
    
        # Catch unexpected errors.
        trap {

            Write-Error $ShortPath

        }
    
        $Output = New-OutputHashtable -Owner $ParentName -Log $LogFile

        $Reader = New-Object -TypeName LogReader -ArgumentList $LogFile
    
        $Line = $Reader.ReadLine(0).Trim()
        if ( -not $Line.StartsWith('#') ) {

            Write-Warning "$ShortPath - Cannot process log with line drawing characters."
            continue

        }

        $Runtime = @{}

        #region Scan
        :Scan do {

            $Line = $Reader.ReadLine(0)

            # Check for scan header corruption that happened in some versions when scanning BIOS drives.
            # https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Peter%20Blaise/94.LOG
            if ( $Line -match '\|\s+\|' ) {

                $Output['Scan Header BIOS Corruption'] = $true
                $Line = $Reader.ReadLine(0) -replace '\|', '+'

            }

            # Skip corrupt manual benchmarks.
            # https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Peter%20Blaise/98.LOG
            if ( $Line -match '^=+$' ) {

                $Line = $Reader.ReadLine(0).Trim('| ')
        
                if ( $Line -eq "Drive's Current Measured Performance" ) {

                    do {

                        $Line = $Reader.ReadLine(0).Trim('| ')

                    } until ( $Line -match '^=+$' )

                    Write-Verbose "$ShortPath - Skipped corrupt manual benchmark."

                    $Line = $Reader.ReadLine(0).Trim('| ')

                    if ( $Reader.EndOfFile() ) {

                        continue File

                    } else {

                        continue Scan

                    }

                } elseif ( $Line.StartsWith('SpinRite 6.1') ) {
            
                    # Skip corrupt logs that are missing their | borders. It would be really complicated to parse them.
                    # https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Colby%20Bouma%20-%20Alpha/728.LOG
                    # This is caused by deferred logging:
                    # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/436
                    Write-Warning "$ShortPath - Skipping corrupt log"
                    continue File

                } else {

                    Write-Error "$ShortPath - Unknown corruption detected"

                }

            }

            # Remove the pipes and spaces from the beginning and end of the line.
            $Line = $Line.Trim('| ')

            # Find the version number. Present in logs from Alpha 9 or later.
            # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/108
            #region Version
            if ( $Line.StartsWith('# SpinRite Detailed Technical Log') ) {

                do {

                    $Line = $Reader.ReadLine(0).Trim()

                } until ( -not $Line.StartsWith('#') )

                if ( $Line ) {

                    $SplitLine = $Line -split '\s'

                    if ( $SplitLine[0] -match 'srpr-dt\d' ) {

                        # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/287#note_3946
                        $Output['Release Type'] = 'diagnostic trace'

                    } else {

                        $Output['Release Type'] = $SplitLine[0..($SplitLine.Count - 2)] -join ' '

                    }

                    # Remove the decorators from special releases.
                    $Output['Version'] = (($SplitLine[-1] -replace '#|\)') -split '-')[-1]

                    $EVParams = @{
                        'Version'     = $Output['Version']
                        'ReleaseType' = $Output['Release Type']
                        'ShortPath'   = $ShortPath
                    }
                    $Output['Equivalent Version'] = Get-EquivalentVersion @EVParams

                }

                # Skip the empty lines after the version number.
                do {

                    $Line = $Reader.ReadLine(0).Trim('| ')

                } until ( $Line )

            }

            # Check for corruption that skips most of a scan.
            # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/267
            if ( $Line.StartsWith('thstndrdJanFeb') ) {

                Write-Verbose "$ShortPath - Skipping thstndrdJanFeb corrupted scan"
                
                do {

                    $Line = $Reader.ReadLine(0).Trim('| ')

                } until ( $Line -match '\+=+\+' )

                $ScanCount ++

            }

            # Added in Alpha 28.
            if ( $Line.StartsWith('Command line') ) {

                $Output['Command Line'] = ($Line -split ': ', 2)[1]

                # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/601
                if ( ($Line -match 'diags') -and ($Line -match 'graphic') ) {

                    Write-Warning "$ShortPath - Cannot process log with line drawing characters (issue 601)."
                    continue File

                }

                $Line = $Reader.ReadLine(1).Trim('| ')

            }

            # Present in logs from Alpha 9 or later.
            # Contains the same information that you would get from running "srpr list".
            #region Drive List
            if ( $Line.StartsWith('Type') ) {
            
                $Line = $Reader.ReadLine(1).Trim('| ')

                while ( $Line ) {

                    $SplitLine = ($Line -split '\|').Trim()

                    if ( $SplitLine[3] -ne '...' ) {

                        $RuntimeRaw = [Int32]($SplitLine[3] -replace '>1M', 1000000)
                        $SerialNumber = $SplitLine[6]

                        # The 'serial number' field gets truncated to 17 characters, so this key needs to match.
                        if ( $SerialNumber.Length -gt 17 ) {

                            $SerialNumber = $SerialNumber.SubString(0, 17)

                        }
                    
                        # This will get matched to each scanned drive later on.
                        $Runtime[$SerialNumber] = $RuntimeRaw

                    }

                    if ( $SplitLine[5] -eq 'This is the system boot drive' ) {

                        $Output['Boot Drive Port'] = [Int32]$SplitLine[2]
                        
                        $BootDriveSizeRaw = $SplitLine[4]
                        $Output['Boot Drive Size'] = $BootDriveSizeRaw

                        $BootDriveSizeNumber = [Double]$BootDriveSizeRaw.SubString(0, ($BootDriveSizeRaw.Length - 1))
                        $BootDriveSizeSuffix = $BootDriveSizeRaw[-1]
                        $BootDriveSizeMultiplier = switch ($BootDriveSizeSuffix) {
                            'K' { 1000; break }
                            'M' { 1000000; break }
                            'G' { 1000000000; break }
                            'T' { 1000000000000; break }
                            'P' { 1000000000000000; break }
                            default { Write-Error "$ShortPath - Unknown boot drive size suffix - '$BootDriveSizeSuffix' - '$BootDriveSizeRaw'" }
                        }
                        $Output['Boot Drive Size Bytes'] = $BootDriveSizeNumber * $BootDriveSizeMultiplier

                    }

                    $Line = $Reader.ReadLine(0).Trim('| ')

                }

                # The log can end here.
                if ( $Reader.EndOfFile() ) {
                    
                    continue File
                
                }
            
                $Line = $Reader.ReadLine(0)

                # Check for scan header corruption that happened in some versions when scanning BIOS drives.
                # https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Chuck%20B%20-%20MSDOS6/102.LOG
                if ( $Line -match '\|\s+\|' ) {

                    $Output['Scan Header BIOS Corruption'] = $true
                    $Line = $Reader.ReadLine(0) -replace '\|', '+'

                }

                $Line = $Line.Trim('| ')

            }

            #region Scan Boundary
            if ( $Line -match '\+=+\+' ) {

                # End of the log.
                if ( $Reader.EndOfFile() ) {

                    [PSCustomObject]$Output
                    continue File

                }
            
                $Line = $Reader.ReadLine(0).Trim('| ')

                if ( -not $Line ) {
                    # End of scan.

                    [PSCustomObject]$Output

                    # The "end of file" character was removed in Alpha 9.
                    # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/109
                    if ( $Reader.EndOfFile() ) {

                        continue File
    
                    }
                
                    # There's only 1 empty line between scans in Alpha 9.
                    if ( -not $Output['Version'] ) {

                        $Line = $Reader.ReadLine(0).Trim('| ')

                    }

                    # If the log contains multiple scans, start a new $Output, but keep Release Type and Version.
                    if ( $Reader.EndOfFile() ) {

                        continue File
    
                    } else {

                        $Output = New-OutputHashtable -Owner $ParentName -Log $LogFile -OldTable $Output

                    }

                } else {
                    # Beginning of scan... usually.

                    # Skip manual benchmarks. Logging of manual benchmarks was added in Alpha 19.
                    if ( $Line -eq "Drive's Current Measured Performance" ) {

                        do {

                            $Line = $Reader.ReadLine(0).Trim('| ')

                        } until ( ($Line -match '\+=+\+') -or ($Reader.EndOfFile()) )

                        $Output['Manual Benchmark Count'] ++

                        $Line = $Reader.ReadLine(0).Trim('| ')

                        if ( $Reader.EndOfFile() ) {

                            continue File
    
                        } else {

                            continue Scan

                        }

                    }

                    $ScanCount ++
                    $Output['Scan'] = $ScanCount

                    $Output['Level'] = [Int32]($Line | Select-String 'level (\d)').Matches.Groups[1].Value
                    $StartTime = ($Line | Select-String 'operation at (.+)\.').Matches.Groups[1].Value.Trim()

                    # Convert "Start Time" to a DateTime object so you can format it however you want.
                    $Output['Start Time'] = Get-Date ($StartTime -replace 'on |,|st|nd|rd|th') -Format $DateFormat

                    $Line = $Reader.ReadLine(1).Trim('| ')

                    # Added in Alpha 10.
                    if ( $Line.StartsWith('From') ) {

                        $SplitLine = ($Line -split '\s+').Trim('%')
                    
                        # 'From %' should always match 'Start %', but re-using the field would be confusing.
                        $Output['Start %'],
                        $Output['From %'],
                        $Output['From Sector'],
                        # 'To %' won't match 'End %' if the scan is interrupted, so it gets its own field.
                        $Output['To %'],
                        $Output['To Sector'] = [Double[]]$SplitLine[1, 1, 3, 5, 7]

                        $Line = $Reader.ReadLine(0).Trim('| ')

                    }

                    # This is used later on to track if the Level was changed during a scan, so it needs to be reset
                    # for each scan.
                    $LevelChanged = $false

                }

            }



            #region Drive Info
            if ( -not $Output['Model'] ) {

                # Look for the divider that is made entirely of "=".
                if ( $Line -match '^=+$' ) {

                    # Alpha 9 got rid of the empty line above the model number.
                    if ( -not $Output['Version'] ) {
                
                        $Line = $Reader.ReadLine(0).Trim('| ')
                        if ( $Line ) {

                            Write-Error "$ShortPath - Line above Model was not empty!"
                            [PSCustomObject]$Output
                            continue File

                        }

                    }

                    $Output['Model'] = $Reader.ReadLine(0).Trim('| ')

                    # Lines aren't always 78 characters, such as in 'Colby Bouma/728.LOG'.
                    $Line = $Reader.ReadLine(0)
                    $LengthOriginal = $Line.Length
                    $Line = $Line.TrimStart('| ')
                    $Key1Length = 40 + ($LengthOriginal - $Line.Length)
                    $Key2Length = $LengthOriginal - $Key1Length

                    $Line = $Reader.ReadLine(0)



                    do {

                        # Split the drive info lines into key:value pairs so they can be processed individually.

                        # Split the line based on where the 2nd key:value pair *should* start.
                        # Trim carefully so empty values don't get deleted, otherwise the split won't work.
                        $Key1 = $Line.Substring(0, $Key1Length).TrimStart('| ')
                        $Key2 = $Line.Substring($Key1Length, $Key2Length).TrimEnd('|')

                        # Split up the key:value pairs.
                        $KeyPair1 = ($Key1 -split ': ').Trim()
                        $KeyPair2 = ($Key2 -split ': ').Trim()
                        $KeyPair3 = $null

                        if ( $KeyPair2.Count -eq 3 ) {
                        
                            # As of Alpha 9, lines can have 3 key:value pairs. The third is the second line of 'features'.
                            $KeyPair2, $KeyPair3 = $KeyPair2[0, 1], ('', $KeyPair2[2])
                        
                        }

                        # Added in RC 5.01.
                        if ( $KeyPair2[0] -eq '4Ksec features' ) {

                            $KeyPair2[0] = 'features'
                            $Output['4K Sectors'] = $true

                        }
                    
                        # Join the pairs into 1 array.
                        $KeyPairs = $KeyPair1, $KeyPair2, $KeyPair3

                        # Process each key:value pair in the drive info section.
                        :Keys foreach ( $KeyPair in $KeyPairs ) {

                            # Skip empty sections.
                            if ( -not $KeyPair ) {

                                continue Keys

                            }
                        
                            $Key, $Value = $KeyPair
                        
                            switch ($Key) {
                                'drive number' {
                                    # "drive number" shows up when the drive's Type is BIOS.
                                    $Key = 'bios drive'

                                    if ( $Output['Equivalent Version'] -ge [SemVer]'6.1.0-rc5.07' ) {

                                        $Value, $Output['BIOS Bugs'] = ($Value -split '\s+', 2).Trim()

                                    }

                                    break
                                }
                                '' {
                                    # "features" is split across 2 lines, so it has to be stitched together.
                                    $Output['features'] += " $Value"
                                    continue Keys
                                }
                                { $_ -match '(ahci|ata|extd|ide) speed' } {
                                    # The name of this field is different for each "access mode", so I renamed it.
                                    $Key = 'Mini Benchmark Speed'
                                    # I convert it to MB/s to keep it consistent with the rest of the log.
                                    $Value = [Math]::Round(([Double]($Value -replace 'byte/s') / 1000000), 3)
                                    break
                                }
                                'max transfer' {
                                    # "max transfer" shows up when the drive's Type is ATA or IDE.
                                    $Key = 'max driv speed'
                                    break
                                }
                                { $_ -in 'byte count', 'sector count' } {
                                    $Value = [Int64]$Value
                                    break
                                }
                                'transfers' {
                                    $Value = [Int32]($Value -replace 'sector')
                                    break
                                }
                                { $_ -in 'attached port', 'phys cylinders', 'physical heads', 'physical sects', 'sector bytes' } {
                                    if ( $Value ) {

                                        $Value = [Int32]$Value

                                    }
                                    break
                                }
                                'serial number' {
                                    $Output['Runtime'] = $Runtime[$Value]
                                }
                            }

                            # Make sure the parsed key is a known value. This will break if I missed something :)
                            if ( $Key -in $Output.Keys ) {

                                $Output[$Key] = $Value

                            } else {

                                Write-Error "$ShortPath - Unknown key '$Key'"
                                [PSCustomObject]$Output
                                continue File

                            }

                        }

                        $Line = $Reader.ReadLine(0)

                    } until ( $Line.Trim('| ') -match '^=+$' )

                    if ( $Output['Equivalent Version'] -ge [SemVer]'6.1.0-rc5.01' ) {

                        if ( -not $Output['4K Sectors'] ) {

                            $Output['4K Sectors'] = $false

                        }

                    }

                    if ( $Output['To %'] -eq 100 ) {

                        $Output['Sector Count Difference'] = $Output['sector count'] - $Output['To Sector']

                    }
                    
                    if ( $Reader.EndOfFile() ) {

                        [PSCustomObject]$Output
                        continue File

                    }

                    $Line = $Reader.ReadLine(0).Trim('| ')

                }

            }



            # "Performance Benchmark" changed to "Drive's measured performance" in Alpha 19.
            #region Before Benchmark
            if ( ($Line -eq 'Performance Benchmarks') -or $Line.StartsWith("Drive's measured performance before") ) {
            
                $null = $Reader.ReadLine(0)

                # Convert the benchmark results to Double, if they aren't null.
                $PerfData = @()
                for ( $Count = 1; $Count -le 5; $Count ++ ) {

                    $PerfLine = ($Reader.ReadLine(0) | Select-String '\d+\.\d+').Matches.Value
                    if ( $PerfLine ) {

                        $PerfLine = [Double]$PerfLine

                    }
                    $PerfData += $PerfLine

                }

                $Output['Before SMART Delay'],
                $Output['Before Random Sectors'],
                $Output['Before Front'],
                $Output['Before Midpoint'],
                $Output['Before End'] = $PerfData

                $Line = $Reader.ReadLine(1).Trim('| ')

            }



            #region Events
            if ( $Line.StartsWith('Event') -or $Line.StartsWith('Sector') ) {

                # Alpha 9 removed the "Event" header, and the error messages now start with "Sector".
                if ( $Line.StartsWith('Event') ) {

                    $null = $Reader.ReadLine(0)

                    # Alpha 24 restored the Events header.
                    if ( $Line -eq 'Events') {

                        $AlphaNinePlus = $true
                        $FirstError = $true
                        $Line = $Reader.ReadLine(0).Trim('| ')

                    } else {

                        $AlphaNinePlus = $false

                    }

                } else {

                    $AlphaNinePlus = $true
                    $FirstError = $true

                }

                do {

                    if ( $AlphaNinePlus ) {

                        # This "do" loop always ends on a divider, but Alpha 9+ logs do not have a header, so they start
                        # on the message itself. Therefore, this loop cannot start with a ReadLine for them.
                        if ( -not $FirstError ) {

                            $Line = $Reader.ReadLine(0).Trim('| ')

                            # I have a log file that got corrupt in this section.
                            # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/238
                            if ( -not $Line ) {

                                Write-Warning "$ShortPath - Corrupt Event section detected."
                                [PSCustomObject]$Output
                                continue File

                            }

                        }
                        $FirstError = $false
                    
                        if ( $Line.StartsWith('Longest') ) {

                            $SplitLine = $Line -split ':'
                            $Output['Longest Reset'] = [Int32]($SplitLine[1].Trim())
                            $Line = $Reader.ReadLine(0).Trim('| ')

                            continue

                        } elseif ( $Line.StartsWith('Count of over-temperature') ) {

                            $SplitLine = $Line -split ':'
                            $Output['Temperature Events'] = [Int32]($SplitLine[1].Trim() -replace '\.')
                            $Line = $Reader.ReadLine(0).Trim('| ')

                            continue

                        } else {

                            $SplitLine = $Line -split '\s+', 4
                            [Double]$SectorPercent = $SplitLine[2] -replace '\(|\)|%'
                            $ErrorText = $SplitLine[3]
                            $Line = $Reader.ReadLine(0).Trim('| ')

                        }


                    } else {

                        $Line = ($Reader.ReadLine(0).Trim('| ') -split '\+')[0]

                        # There can be a divider on top of the first message.
                        if ( $Line.StartsWith('---') ) {

                            $Line = ($Reader.ReadLine(0).Trim('| ') -split '\+')[0]

                        }

                        $ErrorText, [Int64]$SectorNumber = ($Line -split '\|').Trim()
                        $SectorPercent = [Math]::Round( (($SectorNumber / $Output['sector count']) * 100), 4)
                        $Line = ($Reader.ReadLine(0).Trim('| ') -split '\+')[0]

                    }
                
                    while ( (-not $Line.StartsWith('---')) -and ($Line -notmatch '^=+$') -and (-not $Output['Missing Events Divider']) ) {

                        # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/238
                        # https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Peter%20Blaise/326.LOG
                        if ( -not $Line ) {

                            Write-Warning "$ShortPath - Corrupt Event section detected."
                            [PSCustomObject]$Output
                            continue File

                        }
                        
                        # The divider is frequently missing for the over-temperature event.
                        if ( $Line.StartsWith('Count of over-temperature') ) {

                            $SplitLine = $Line -split ':'
                            $Output['Temperature Events'] = [Int32]($SplitLine[1].Trim() -replace '\.')

                        } else {
                        
                            $ErrorText += " $($Line.Trim())"

                        }

                        # I have a log that just cuts off in the error section.
                        $Line = $Reader.ReadLine(0)
                        if ( $Reader.EndOfFile() ) {

                            [PSCustomObject]$Output
                            continue File

                        } else {

                            $Line = ($Line.Trim('| ') -split '\+')[0]

                            # The divider between Events and the After benchmark can be missing in Alpha 21.
                            # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/340
                            if ( $Line.StartsWith("Drive's measured performance") ) {

                                $Output['Missing Events Divider'] = $true

                            }

                        }

                    }

                    # Set a flag to skip calculating Average Speed if the Level was changed during the scan,
                    # unless it was changed between 1 and 2.
                    if ( $ErrorText.StartsWith('SpinRite operating level has been set to level') ) {

                        if ( -not (($Output['Level'] -in 1, 2) -and (($ErrorText -split ':').Trim('. ')[-1] -in 1, 2 )) ) {

                            $LevelChanged = $true

                        }

                    }

                    # Grab 'End %' from Events if SpinRite is interrupted.
                    if ( $ErrorText -eq "SpinRite's operation on this drive has been interrupted by its user." ) {

                        $Output['End %'] = $SectorPercent

                    }

                    $Output['Errors'][$ErrorText] ++

                } until ( ($Line -match '^=+$') -or $Output['Missing Events Divider'] )

                if ( -not $Output['Missing Events Divider'] ) {
            
                    $Line = $Reader.ReadLine(0).Trim('| ')

                }

            }



            #region After Benchmark
            if ( $Line.StartsWith("Drive's measured performance after") ) {
            
                $null = $Reader.ReadLine(0)

                # Convert the benchmark results to Double, if they aren't null.
                $PerfData = @()
                for ( $Count = 1; $Count -le 5; $Count ++ ) {

                    $PerfLine = ($Reader.ReadLine(0) | Select-String '\d+\.\d+').Matches.Value
                    if ( $PerfLine ) {

                        $PerfLine = [Double]$PerfLine

                    }
                    $PerfData += $PerfLine

                }

                $Output['After SMART Delay'],
                $Output['After Random Sectors'],
                $Output['After Front'],
                $Output['After Midpoint'],
                $Output['After End'] = $PerfData

                # The divider can be missing after the After benchmark.
                # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/303
                $Line = $Reader.ReadLine(0).Trim('| ')
                if ( $Line -match '^=+$' ) {

                    $Line = $Reader.ReadLine(0).Trim('| ')

                }

            }



            #region GSD
            if ( $Line.Contains('Graphic Status Display') ) {

                $Line = $Reader.ReadLine(1).Trim('| ')
            
                $GsdBlocks = $null
                do {

                    $GsdBlocks += $Line
                    $Line = $Reader.ReadLine(0).Trim('| ')

                } until ( $Line.StartsWith('---') -or $Reader.EndOfFile() )

                $Output['GSD Block Count'] = $GsdBlocks.Length

            }



            if ( $Line.StartsWith('--- work') ) {

                $Line = $Reader.ReadLine(0).Trim('| ')
                $SplitLine = $Line -split '\s+'

                # The 'After' benchmark annihilates the GSD numbers.
                # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/302
                # https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Colby%20Bouma%20-%20Alpha/643.LOG
                if ( $Line.StartsWith('megabytes') ) {

                    $RemainingMB = [Double]$SplitLine[1]
                    $CompletedMB = [Double]$SplitLine[2]

                } else {

                    $RemainingMB = [Double]$SplitLine[0]
                    $CompletedMB = [Double]$SplitLine[1]

                }

                $Output['Remaining MB'] = $RemainingMB

                if ( $Output['Equivalent Version'].Major -eq 9 ) {

                    $Output['Location MB'] = $CompletedMB

                } else {

                    $Output['Completed MB'] = $CompletedMB

                }

                $Line = $Reader.ReadLine(0).Trim('| ') -split '\s+'

                # Added in Alpha 28.
                if ( $Line[0] -match '(\d+\.\d+)%:' ) {

                    $Output['Location %'] = [Double]$Matches[1]

                }

                $Output['Remaining %'] = [Double]($Line[1] -replace '\||%')

                if ( $Output['Equivalent Version'].Major -eq 9 ) {

                    $Output['Location %'] = [Double]($Line[2] -replace '\||%')

                } else {

                    $Output['Completed %'] = [Double]($Line[2] -replace '\||%')

                }
            
                $UnprocessedCharacter = $Line[3]

                $Line = $Reader.ReadLine(0).Trim('| ') -split '\s+'
                $Output['Remaining Time'] = ConvertFrom-LargeTimeSpan ($Line[1] -replace '--:--:--', '00:00:00')
                $Output['Completed Time'] = ConvertFrom-LargeTimeSpan ($Line[2] -replace '--:--:--', '00:00:00')

                $Line = $Reader.ReadLine(1).Trim('| ')



                $UnprocessedCount = ([Char[]]$GsdBlocks | Group-Object | Where-Object Name -eq $UnprocessedCharacter).Count
                $GsdCompletedPercent = [Math]::Round( (($GsdBlocks.Length - $UnprocessedCount) / $GsdBlocks.Length), 5)
                $Output['GSD Completed %'] = ($GsdCompletedPercent * 100)

                # Calculate where the scan started and ended, based on the GSD, because "completed %" can be wrong in some cases.
                $GsdSplit = $GsdBlocks -split "[^$UnprocessedCharacter]+"
                $StartLength = $GsdSplit[0].Length
                $EndLength = $GsdSplit[1].Length

                # Start != 0, End != 100
                if ( $GsdSplit[0] -and $GsdSplit[1] ) {

                    $StartPercent = [Math]::Round( (($StartLength / $GsdBlocks.Length) * 100), 3)
                    $UnprocessedTotal = $StartLength + $EndLength
                    $ProcessedLength = $GsdBlocks.Length - $UnprocessedTotal
                    $ProcessedEnd = $ProcessedLength + $StartLength
                    $EndPercent = [Math]::Round( (($ProcessedEnd / $GsdBlocks.Length) * 100), 3)

                }

                # Start = 0, End != 100
                if ( (-not $GsdSplit[0]) -and $GsdSplit[1] ) {

                    $StartPercent = 0
                    $EndPercent = [Math]::Round( ((($GsdBlocks.Length - $EndLength) / $GsdBlocks.Length) * 100), 3)

                }

                # Start = 0, End = 100
                if ( (-not $GsdSplit[0]) -and (-not $GsdSplit[1]) ) {

                    $StartPercent = 0
                    $EndPercent = 100

                }

                # Start != 0, End = 100
                if ( $GsdSplit[0] -and (-not $GsdSplit[1]) ) {

                    $StartPercent = [Math]::Round( (($StartLength / $GsdBlocks.Length) * 100), 3)
                    $EndPercent = 100

                }

                # Don't set 'Start %' if it has already been copied from 'From %'.
                if ( -not $Output['Start %'] ) {

                    $Output['Start %'] = $StartPercent
                    $StartPercentFromGsd = $true

                } else {

                    $StartPercentFromGsd = $false

                }
                # Don't set 'End %' if it has already been grabbed from Events.
                if ( -not $Output['End %'] ) {

                    $Output['End %'] = $EndPercent
                    $EndPercentFromGsd = $true

                } else {

                    $EndPercentFromGsd = $false

                }

                # Skip Average Speed if it cannot be accurately calculated.
                if ( $LevelChanged ) {

                    $Output['Average Speed Skipped'] = 'Level changed'

                }

                # Check for broken GSD: https://dev.grc.com/Steve/spinrite-v6.1/-/issues/138
                if ( $GsdSplit.Count -gt 2 ) {

                    # Only erase these if they were calculated from the GSD.
                    if ( $StartPercentFromGsd ) {
            
                        $Output['Start %'] = $null
            
                    }
                    if ( $EndPercentFromGsd ) {
            
                        $Output['End %'] = $null
            
                    }

                    $Output['Average Speed Skipped'] = 'Broken GSD'
            
                }

                # The "After" benchmark breaks all the GSD numbers in Alpha 19.
                # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/302
                if ( ($Output['Equivalent Version'].Major -eq 19) -and ($Output['After Random Sectors']) ) {

                    $Output['Average Speed Skipped'] = 'After benchmark'

                }

                if ( -not $Output['Average Speed Skipped'] ) {

                    # Calculate the total amount of data transferred during the scan.
                    # These multipliers are supposed to be 3 and 5, but it looks like they are currently 2 and 4.
                    # https://grc.com/groups/spinrite.dev:42147
                    $TrueCompletedMB = $Output['byte count']
                    switch ($Output['Level']) {
                        3 {
                            # Read, write.
                            $TrueCompletedMB *= 2
                            break
                        }
                        4 {
                            if ( $Output['Equivalent Version'] -ge [SemVer]'6.1.0-a28.0' ) {

                                # Read, write, read.
                                $TrueCompletedMB *= 3

                            } else {

                                # Read, write inverted, read, write original.
                                $TrueCompletedMB *= 4

                            }
                            break
                        }
                        # Introduced in Alpha 28.
                        5 {
                            # Read, write inverted, read, write original, read.
                            $TrueCompletedMB *= 5
                        }
                    }

                    $TrueCompletedMB = ($TrueCompletedMB / 1000000)

                    # Only use $GsdCompletedPercent if the difference between it and 'Completed %' is greater than the
                    # resolution of 1 block.
                    $CompletedDifference = [Math]::Abs(($Output['GSD Completed %'] - $Output['Completed %']))
                    $CompletedDifference = [Math]::Round($CompletedDifference, 3)
                    $Output['Completed Difference'] = $CompletedDifference
                    $BlockResolution = (1 / $GsdBlocks.Length) * 100
                    $Output['Block Resolution'] = $BlockResolution
                    if ( $CompletedDifference -gt $BlockResolution) {

                        $TrueCompletedMB = $TrueCompletedMB * $GsdCompletedPercent

                    } else {

                        $TrueCompletedMB = $TrueCompletedMB * ($Output['Completed %'] / 100)

                    }
            
                    $Output['Average Speed'] = [Math]::Round(($TrueCompletedMB / ($Output['Completed Time']).TotalSeconds), 3)

                }

            }



            #region Event Counts
            if ( $Line -eq 'Final Sector Event Counts' ) {

                # 529.LOG has a bunch of 0x00, in addition to spaces for some bizarre reason.
                $Line = $Reader.ReadLine(1).Trim('|', ' ', [Char]0)
                $SplitLine = (($Line -split ':').Trim() -split '\s+').Trim()

                # Present in logs from Alpha 9 or later.
                # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/177
                if ( $Line.StartsWith('command timeout') ) {

                    $Output['Command Timeout'] = [Int64]$SplitLine[2]
                    $Output['Command Aborted'] = [Int64]$SplitLine[5]
                    $SplitLine = (($Reader.ReadLine(0).Trim('|', ' ', [Char]0) -split ':').Trim() -split '\s+').Trim()

                }
            
                $Output['Comm/Cable Errors'] = [Int64]$SplitLine[2]
                $Output['Not Recoverable'] = [Int64]$SplitLine[5]
                $SplitLine = (($Reader.ReadLine(0).Trim('|', ' ', [Char]0) -split ':').Trim() -split '\s+').Trim()

                $Output['Minor Troubles'] = [Int64]$SplitLine[2]
                $Output['Sector Never Found'] = [Int64]$SplitLine[5]
                $SplitLine = (($Reader.ReadLine(0).Trim('|', ' ', [Char]0) -split ':').Trim() -split '\s+').Trim()

                $Output['DynaStat Recovered'] = [Int64]$SplitLine[2]
                $Output['Defective Sector'] = [Int64]$SplitLine[5]
            
                $Line = $Reader.ReadLine(1).Trim('| ')

            }



            #region SMART
            if ( $Line -eq 'End-of-Run SMART System Status' ) {

                $SmartNames = @(
                    'ECC Corrected'
                    'Read Channel Margin'
                    'Relocated Sectors'
                    'Reallocation Events'
                    'Seek Errors'
                    'Recalibration Retries'
                    'Cabling Errors'
                    'Uncorrectable'
                    'Write Errors'
                )
            
                $RateNames = @(
                    'ECC Corrected'
                    'Reallocation Events'
                    'Seek Errors'
                    'Recalibration Retries'
                    'Cabling Errors'
                    'Uncorrectable'
                    'Write Errors'
                )
                
                # The rates section was replaced with 8 new attributes in SRPR-037.
                # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/438
                if ( $Output['Equivalent Version'] -ge [SemVer]'6.1.0-a28.5' ) {

                    $SmartNames += @(
                        'Command Timeout'
                        'Pending Sectors'
                        'Read Retries'
                        'Total Writes'
                        'Write Failures'
                        'Wear Leveling'
                        'Remaining Life'
                        'Reallocation Space'
                    )

                    $RateNames = @()

                }
                
                $Line = $Reader.ReadLine(2).Trim('| ')

                # Parse the SMART health and raw data.
                foreach ( $SmartName in $SmartNames ) {

                    $SplitSmart = $Line -split '\s+'

                    # If the last character is -, there's no SMART data.
                    if ( $SplitSmart[-1] -ne '-' ) {

                        # If there's data on the line, the "raw data" field will be present, and should always be 14 characters.
                        if ( $SplitSmart[-1].Length -eq 14 ) {

                            $Output["$SmartName Raw"] = $SplitSmart[-1]

                        } else {

                            Write-Error "$ShortPath - Unable to parse SMART: '$Line'"

                        }

                        # "current/max" aren't always present, even if "raw data" is.
                        if ( $SplitSmart[-2].Contains('/') ) {

                            [Int32]$Output["$SmartName Current"],
                            [Int32]$Output["$SmartName Max"] = $SplitSmart[-2] -split '\/'

                            # Letting 0 be null creates a greater visual difference.
                            $SmartDiff = $Output["$SmartName Max"] - $Output["$SmartName Current"]
                            if ( $SmartDiff -gt 0 ) {

                                $Output["$SmartName Diff"] = $SmartDiff

                            }

                        }

                        # Search for missing slash (/), like in:
                        # https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Colby%20Bouma%20-%20Alpha/349.LOG
                        # https://gitlab.com/GRC-Community/SpinRite-Logs/-/blob/main/6.1/Colby%20Bouma%20-%20Alpha/816.LOG
                        # https://grc.com/groups/spinrite.dev:46321
                        if ( -not $Output['Missing Slash'] ) {

                            try {

                                $null = [Int32]$SplitSmart[-2]
                                $Output['Missing Slash'] = $true

                            } catch {}

                        }

                    }

                    $Line = $Reader.ReadLine(0).Trim('| ')

                }

                if ( $Line.StartsWith('-temp') ) {

                    $Temperature = [Int32[]]($Line | Select-String 'temp (\d+).+?(\d+)').Matches.Groups[1, 2].Value
                    $Output['Temperature Celsius'], $Output['Temperature Fahrenheit'] = $Temperature

                }

                # SRPR-038 (Alpha 28.6) changed how temperature is formatted.
                # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/460
                if ( $Line -match '^-+$' ) {

                    $Line = $Reader.ReadLine(0).Trim('| ')

                    $Temperature = [Int32[]]($Line | Select-String 'temperature: (\d+).+?(\d+)').Matches.Groups[1, 2].Value
                    $Output['Temperature Celsius'], $Output['Temperature Fahrenheit'] = $Temperature

                    $Line = $Reader.ReadLine(0).Trim('| ')

                }

                # SRPR-037 (Alpha 28.5) changed the SMART page.
                # https://dev.grc.com/Steve/spinrite-v6.1/-/issues/438
                if ( $Output['Equivalent Version'] -lt [SemVer]'6.1.0-a28.5' ) {
                
                    $Line = $Reader.ReadLine(0).Trim('| ')

                }

                # Parse the SMART rates.
                foreach ( $RateName in $RateNames ) {

                    # Remove the space from names with 2 words.
                    $SplitRate = $Line -replace '([a-z])\s([a-z])', "`$1`$2"
                    $SplitRate = $SplitRate -split '\s+'

                    switch ($SplitRate.Count) {
                        # As of NTR 025, "error count" defaults to null instead of 0.
                        1 {
                            break
                        }
                        2 {
                            $Output["$RateName Rate Count"] = [Int64]$SplitRate[1]
                            break
                        }
                        3 {
                            $Output["$RateName Rate Count"] = [Int64]$SplitRate[1]
                            $Output["$RateName Rate Current"] = [Int64]$SplitRate[2]
                            break
                        }
                        5 {
                            $Output["$RateName Rate Count"],
                            $Output["$RateName Rate Min"],
                            $Output["$RateName Rate Current"],
                            $Output["$RateName Rate Max"] = [Int64[]]$SplitRate[1..4]
                            break
                        }
                        default {
                            Write-Error "$ShortPath - Unable to parse Rate: '$Line'"
                        }
                    }

                    $Line = $Reader.ReadLine(0).Trim('| ')

                }

                $Line = $Reader.ReadLine(0).Trim('| ')

            }



            #region End Time
            if ( -not $Output['End Time'] ) {

                if ( $Line -match 'operation (\w+) at (.+)\.') {

                    $Output['Stop Type'] = $Matches[1]
                    $Output['End Time'] = Get-Date ($Matches[2].Trim() -replace 'on |,|st|nd|rd|th') -Format $DateFormat

                    $Output['Scan Time'] = [DateTime]$Output['End Time'] - [DateTime]$Output['Start Time']
                    $ScanTimeDifference = ($Output['Scan Time'] - $Output['Completed Time']).TotalSeconds
                    if ( [Math]::Abs($ScanTimeDifference) -gt 60 ) {

                        $Output['Scan Time Difference'] = $ScanTimeDifference

                    }

                }

            }

        } until ( $Reader.EndOfFile() )

        # Catch logs that don't contain a scan.
        if ( ($Output['Level']) -and (-not $Output['GSD Complete %']) ) {

            [PSCustomObject]$Output

        }

    }

    $ScanStopwatch.Stop()
    $LogCount = $_.Value['Logs'].Count
    $ThreadLength = $_.Value['TotalLength'] / 1MB
    $ThreadTime = $ScanStopwatch.Elapsed.TotalSeconds
    'Thread {0} - {1} logs - {2:N1} s - {3:N2} MB' -f $_.Key, $LogCount, $ThreadTime, $ThreadLength | Write-Verbose

} | Sort-Object Owner, Log, Scan

$StopWatch.Stop()
Write-Verbose ([Math]::Round($StopWatch.Elapsed.TotalSeconds, 3))