[CmdletBinding()]
param ()

$Count = @{
    ClosingBrackets = 0
    Comments = 0
    Empty = 0
    Lines = 0
    MultiComments = 0
}
$MultiComment = $false
foreach ( $Line in (Get-Content '.\Import-SpinRite61AlphaLog.ps1') ) {
    
    $TrimmedLine = $Line.Trim()
    
    if ( -not $TrimmedLine ) {
        $Count['Empty'] ++
        continue
    }
    if ( $TrimmedLine -in '}', ') {' ) {
        $Count['ClosingBrackets'] ++
        continue
    }
    if ( $TrimmedLine -eq '<#' ) {
        $MultiComment = $true
        $Count['MultiComments'] ++
        continue
    }
    if ( $MultiComment ) {
        if ( $TrimmedLine -eq '#>' ) {
            $MultiComment = $false
        }
        $Count['MultiComments'] ++
        continue
    }
    if ( $TrimmedLine.StartsWith('#') ) {
        $Count['Comments'] ++
        continue
    }

    $Count['Lines'] ++
}
[PSCustomObject]$Count