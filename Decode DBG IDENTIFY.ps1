#Requires -Version 7

[CmdletBinding()]
param ()

$Version = '021'

<#

Instructions:

- Download this script.
    - https://gitlab.com/GRC-Community/SpinRite-Logs/-/raw/main/Decode%20DBG%20IDENTIFY.ps1?ref_type=heads&inline=false
- Install PowerShell 7.
    - winget install Microsoft.PowerShell
    - or https://github.com/PowerShell/PowerShell/releases
- Open "PowerShell 7", not "Windows PowerShell".
- Copy an IDENTIFY block from a DBG file. There is an example block below.
- Run this script.
    - & '.\Decode DBG IDENTIFY.ps1'

0040 3FFF C837 0010 0000 0000 003F 0000 0000 0000 3139 3336 3031 3133 3031 3239
3620 2020 2020 2020 0000 0000 0000 5330 3730 3441 3020 4249 5749 4E20 5353 4420
2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 8001
4000 2F00 4000 0200 0000 0007 3FFF 0010 003F FC10 00FB 9101 C2B0 0EE7 0000 0007
0003 0078 0078 0078 0078 4D20 0000 0000 0000 0000 0000 001F 850E 0006 0044 0040
03F0 0110 346B 7509 4063 3469 B409 4063 407F 0001 0001 0080 FFFE 0000 0000 0000
0000 0000 0000 0000 C2B0 0EE7 0000 0000 0000 0008 4000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 401C 401C 0000 0000 0000 0000 0000 0000 0000
0029 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000 0007 0001 0000 0000 0000 0000 0000 0000
2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020
2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 2020 0000 0000
0000 4000 0000 0000 0000 0000 0000 0000 0000 0001 0000 0000 0000 0000 10FF 0000
0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0020 0020 0000 0000 0000 0000
0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 03A5



Citations:

I started by reading Microsoft's header file (ata.h):
https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/ata/ns-ata-_identify_device_data

Microsoft's data types:
https://learn.microsoft.com/en-us/windows/win32/winprog/windows-data-types

C++ struct:
https://learn.microsoft.com/en-us/cpp/cpp/struct-cpp?view=msvc-170

C++ bit fields:
https://learn.microsoft.com/en-us/cpp/cpp/cpp-bit-fields?view=msvc-170

Once I finished reading ata.h, I looked through the smartmontools source:
https://www.smartmontools.org/browser/trunk/smartmontools/ataidentify.cpp
#>



class ByteReader {

    [Byte[]]$ByteArray
    [Int32] $ByteArrayIndex
    [UInt16]$CurrentWord
    [Int32] $CurrentWordBitIndex

    # Constructor.
    ByteReader () {

        $ByteString = ((Get-Clipboard) -replace '\s') -join ''
        $this.ByteArray = [System.Convert]::FromHexString($ByteString)
        $this.SetCurrentWord()

    }

    # Methods.

    [Object[]] ConvertToUInt (
        [Int32]$Size,
        [Int32]$Count
    ) {
    
        $IndexMax = ($Size / 8) - 1
        $Result = @()
        
        for ( $CurrentCount = 1; $CurrentCount -le $Count; $CurrentCount ++ ) {
            
            $Bytes = $this.ByteArray[$this.ByteArrayIndex..($this.ByteArrayIndex + $IndexMax)]
    
            # Reverse the words. Thanks for catching this, Steve!
            # https://forums.grc.com/threads/release-candidate-6.1461/post-11077
            if ( $Size -gt 16 ) {
    
                $TempBytes = @()
                $TotalWords = $Size / 16
                $WordIndex = $Bytes.Count - 2
    
                for ( $WordCount = 1; $WordCount -le $TotalWords; $WordCount ++ ) {
    
                    $TempBytes += $Bytes[$WordIndex], $Bytes[($WordIndex + 1)]
                    $WordIndex -= 2
    
                }
    
                $Bytes = $TempBytes
    
            }
    
            if ( [System.BitConverter]::IsLittleEndian ) {
    
                [Array]::Reverse($Bytes)
    
            }

            Write-Verbose "ConvertToUInt - Count: $CurrentCount/$Count - Size: $Size - Index: $($this.ByteArrayIndex) - Bytes: $($Bytes -join ', ')"
    
            $Result += switch ( $Size ) {
                16 { [System.BitConverter]::ToUInt16($Bytes, 0); break }
                32 { [System.BitConverter]::ToUInt32($Bytes, 0); break }
                64 { [System.BitConverter]::ToUInt64($Bytes, 0); break }
            }
    
            $this.ByteArrayIndex += ($IndexMax + 1)
    
        }
    
        return $Result

    }

    [String] GetHex (
        [Int32]$Count
    ) {

        $IndexMax = $this.ByteArrayIndex + ($Count - 1)
        $Bytes = $this.ByteArray[$this.ByteArrayIndex..$IndexMax]
        $this.ByteArrayIndex += $Count
        $this.SetCurrentWord()
        Write-Verbose "GetHex - Index: $($this.ByteArrayIndex) - Count: $Count"

        $HexString = [System.Convert]::ToHexString($Bytes)

        # Add "0x" to make it clear this is a hex string.
        return '0x{0}' -f $HexString

    }

    [UInt16] GetMask (
        [Int32]$BitCount
    ) {

        $Mask = 0
        for ( $Exponent = $this.CurrentWordBitIndex; $Exponent -lt ($this.CurrentWordBitIndex + $BitCount); $Exponent ++ ) {

            $Mask += [Math]::Pow(2, $Exponent)

        }

        return $Mask

    }

    [String] GetString (
        [Int32]$Count
    ) {

        $IndexMax = $this.ByteArrayIndex + ($Count - 1)
        $Bytes = $this.ByteArray[$this.ByteArrayIndex..$IndexMax]
        $this.ByteArrayIndex += $Count
        $this.SetCurrentWord()
        Write-Verbose "GetString - Index: $($this.ByteArrayIndex) - Count: $Count"

        # 0x00 is null. It shouldn't appear in any of the string properties, but it can in broken devices.
        # I replace it with space (0x20) because null causes Out-String to stop early (strings are null-terminated).
        for ( $BytesIndex = 0; $BytesIndex -lt $Bytes.Count; $BytesIndex ++ ) {

            if ( $Bytes[$BytesIndex] -eq 0x00 ) {

                $Bytes[$BytesIndex] = 0x20

            }

        }

        # Turn the bytes into characters, then concatenate them into a string.
        $RawString = [Char[]]$Bytes -join ''

        # Wrap the string in quotes to make spaces visible.
        return "'{0}'" -f $RawString

    }

    [UInt16] GetWord () {

        return $this.GetWord(1)

    }
    
    [String] GetWord (
        [Int32]$Count
    ) {

        $Result = $this.ConvertToUInt(16, $Count)
        $this.SetCurrentWord()
        Write-Verbose "GetWord - Index: $($this.ByteArrayIndex)"
        return $Result -join ', '

    }

    [UInt32] GetDWord () {

        return $this.GetDWord(1)

    }
    
    [String] GetDWord (
        [Int32]$Count
    ) {

        $Result = $this.ConvertToUInt(32, $Count)
        $this.SetCurrentWord()
        Write-Verbose "GetDWord - Index: $($this.ByteArrayIndex)"
        return $Result -join ', '

    }

    [UInt64] GetQWord () {

        return $this.GetQWord(1)

    }
    
    [String] GetQWord (
        [Int32]$Count
    ) {

        $Result = $this.ConvertToUInt(64, $Count)
        $this.SetCurrentWord()
        Write-Verbose "GetQWord - Index: $($this.ByteArrayIndex)"
        return $Result -join ', '

    }

    [UInt16] ReadBits (
        [Int32]$BitCount
    ) {
        
        $Mask = $this.GetMask($BitCount)
        $Result = ($this.CurrentWord -band $Mask) -shr $this.CurrentWordBitIndex
        $this.CurrentWordBitIndex += $BitCount
        
        if ( $this.CurrentWordBitIndex -eq 16 ) {

            $this.CurrentWordBitIndex = 0
            $this.SetNextWord()

        }

        return $Result

    }

    [Bool] ReadBool () {

        Write-Verbose "ReadBool - ByteArrayIndex: $($this.ByteArrayIndex) - CurrentWordBitIndex: $($this.CurrentWordBitIndex)"
        return $this.ReadBits(1)

    }

    [UInt16] ReadInt (
        [Int32]$BitCount
    ) {

        Write-Verbose "ReadInt - ByteArrayIndex: $($this.ByteArrayIndex) - CurrentWordBitIndex: $($this.CurrentWordBitIndex)"
        return $this.ReadBits($BitCount)

    }

    [Void] SetCurrentWord () {

        if ( $this.ByteArrayIndex -lt ($this.ByteArray.Count -2) ) {

            $this.CurrentWord = $this.ConvertToUInt(16, 1)[0]
            $this.ByteArrayIndex -= 2
            Write-Verbose "SetCurrentWord - Index: $($this.ByteArrayIndex)"

        } else {

            Write-Verbose "SetCurrentWord - End of array - Index: $($this.ByteArrayIndex)"

        }

    }
    
    [Void] SetNextWord () {

        $this.ByteArrayIndex += 2
        $this.SetCurrentWord()
        Write-Verbose "SetNextWord - Index: $($this.ByteArrayIndex)"

    }

}



$ByteReader = [ByteReader]::new()
[PSCustomObject]@{
    ScriptVersion                               = $Version
    # Word 0 - General Configuration
    GeneralConfigurationReserved1               = $ByteReader.ReadBool() # 00000000 00000001
    GeneralConfigurationRetired3                = $ByteReader.ReadBool() # 00000000 00000010
    GeneralConfigurationResponseIncomplete      = $ByteReader.ReadBool() # 00000000 00000100
    GeneralConfigurationRetired2                = $ByteReader.ReadInt(3) # 00000000 00111000
    GeneralConfigurationFixedDevice             = $ByteReader.ReadBool() # 00000000 01000000
    GeneralConfigurationRemovableMedia          = $ByteReader.ReadBool() # 00000000 10000000
    GeneralConfigurationRetired1                = $ByteReader.ReadInt(7) # 01111111 00000000
                                                                         # 10000000 00000000
    GeneralConfigurationDeviceType              = switch ( $ByteReader.ReadInt(1) ) {
        0 { 'ATA'; break }
        1 { 'ATAPI'; break }
    }

    NumCylinders                                = $ByteReader.GetWord()     # Word 1
    SpecificConfiguration                       = $ByteReader.GetHex(2)     # Word 2
    NumHeads                                    = $ByteReader.GetWord()     # Word 3
    Retired1                                    = $ByteReader.GetWord(2)    # Word 4-5
    NumSectorsPerTrack                          = $ByteReader.GetWord()     # Word 6
    VendorUnique1                               = $ByteReader.GetWord(3)    # Word 7-9
    SerialNumber                                = $ByteReader.GetString(20) # Word 10-19
    Retired2                                    = $ByteReader.GetWord(2)    # Word 20-21
    Obsolete1                                   = $ByteReader.GetWord()     # Word 22
    FirmwareRevision                            = $ByteReader.GetString(8)  # Word 23-26
    ModelNumber                                 = $ByteReader.GetString(40) # Word 27-46

    # Word 47 - READ/WRITE MULTIPLE support
    MaximumBlockTransfer                        = $ByteReader.ReadInt(8)  # 00000000 11111111
    VendorUnique2                               = $ByteReader.ReadInt(8)  # 11111111 00000000

    # Word 48 - Trusted Computing
    TrustedComputingFeatureSupported            = $ByteReader.ReadBool()  # 00000000 00000001
    TrustedComputingReserved                    = $ByteReader.ReadInt(13) # 00111111 11111110
    TrustedComputingMustBe1                     = $ByteReader.ReadInt(2)  # 11000000 00000000

    # Word 49 - Capabilities
    CurrentLongPhysicalSectorAlignment          = $ByteReader.ReadInt(2)  # 00000000 00000011
    CapabilitiesReservedByte49                  = $ByteReader.ReadInt(6)  # 00000000 11111100
    DmaSupported                                = $ByteReader.ReadBool()  # 00000001 00000000
    LbaSupported                                = $ByteReader.ReadBool()  # 00000010 00000000
    IordyDisable                                = $ByteReader.ReadBool()  # 00000100 00000000
    IordySupported                              = $ByteReader.ReadBool()  # 00001000 00000000
    CapabilitiesReserved1                       = $ByteReader.ReadBool()  # 00010000 00000000
    StandbyTimerSupport                         = $ByteReader.ReadBool()  # 00100000 00000000
    CapabilitiesReserved2                       = $ByteReader.ReadInt(2)  # 11000000 00000000
    # Word 50 - Capabilities
    VendorSpecificMinStandbyTimer               = $ByteReader.ReadInt(1)  # 00000000 00000001
    CapabilitiesReservedOSB6                    = $ByteReader.ReadInt(1)  # 00000000 00000010
    CapabilitiesReserved3                       = $ByteReader.ReadInt(12) # 00111111 11111100
    CapabilitiesMustBe1                         = $ByteReader.ReadInt(2)  # 11000000 00000000

    PIODataTransferMode                         = $ByteReader.GetWord() # Word 51
    SingleWordDMADataTransferMode               = $ByteReader.GetWord() # Word 52

    # Word 53 - Field validity
    Words54to58CHSAreValid                      = $ByteReader.ReadBool() # 00000000 00000001
    Words64to70PIOModesAreValid                 = $ByteReader.ReadBool() # 00000000 00000010
    Word88UltraDMAModesIsValid                  = $ByteReader.ReadBool() # 00000000 00000100
    Reserved3                                   = $ByteReader.ReadInt(5) # 00000000 11111000
    FreeFallControlSensitivity                  = $ByteReader.ReadInt(8) # 11111111 00000000

    NumberOfCurrentCylinders                    = $ByteReader.GetWord()  # Word 54
    NumberOfCurrentHeads                        = $ByteReader.GetWord()  # Word 55
    CurrentSectorsPerTrack                      = $ByteReader.GetWord()  # Word 56
    CurrentSectorCapacity                       = $ByteReader.GetDWord() # Word 57-58

    # Word 59 - Sanitize Device
    MultiSectorSettingValid                     = $ByteReader.ReadBool() # 00000000 00000001
    SanitizeReserved                            = $ByteReader.ReadBool() # 00000000 00000010
    SanitizeAntifreezeLockExtSupported          = $ByteReader.ReadBool() # 00000000 00000100
    SanitizeCmdsACS3                            = $ByteReader.ReadBool() # 00000000 00001000
    SanitizeFeatureSupported                    = $ByteReader.ReadBool() # 00000000 00010000
    CryptoScrambleExtCommandSupported           = $ByteReader.ReadBool() # 00000000 00100000
    OverwriteExtCommandSupported                = $ByteReader.ReadBool() # 00000000 01000000
    BlockEraseExtCommandSupported               = $ByteReader.ReadBool() # 00000000 10000000
    CurrentNumSectorsPerDRQ                     = $ByteReader.ReadInt(8) # 11111111 00000000

    UserAddressableSectors                      = $ByteReader.GetDWord() # Word 60-61
    SingleWordDMAModes                          = $ByteReader.GetWord()  # Word 62

    # Word 63 - MultiWord DMA modes
    MultiWordDMASupportMode0                    = $ByteReader.ReadBool() # 00000000 00000001
    MultiWordDMASupportMode1                    = $ByteReader.ReadBool() # 00000000 00000010
    MultiWordDMASupportMode2                    = $ByteReader.ReadBool() # 00000000 00000100
    MultiWordDMASupportReserved                 = $ByteReader.ReadInt(5) # 00000000 11111000
    MultiWordDMAActiveMode0                     = $ByteReader.ReadBool() # 00000001 00000000
    MultiWordDMAActiveMode1                     = $ByteReader.ReadBool() # 00000010 00000000
    MultiWordDMAActiveMode2                     = $ByteReader.ReadBool() # 00000100 00000000
    MultiWordDMAActiveReserved                  = $ByteReader.ReadInt(5) # 11111000 00000000

    # Word 64 - PIO modes
    AdvancedPIOSupportMode3                     = $ByteReader.ReadBool()  # 00000000 00000001
    AdvancedPIOSupportMode4                     = $ByteReader.ReadBool()  # 00000000 00000010
    AdvancedPIOReserved                         = $ByteReader.ReadInt(14) # 11111111 11111100

    MinimumMWXferCycleTimeNs                    = $ByteReader.GetWord() # Word 65
    RecommendedMWXferCycleTimeNs                = $ByteReader.GetWord() # Word 66
    MinimumPIOCycleTimeNs                       = $ByteReader.GetWord() # Word 67
    MinimumPIOCycleTimeIORDYNs                  = $ByteReader.GetWord() # Word 68

    # Word 69 - Additional Support
    ZonedCapabilities                           = $ByteReader.ReadInt(2) # 00000000 00000011
    NonVolatileWriteCache                       = $ByteReader.ReadBool() # 00000000 00000100
    ExtendedUserAddressableSectorsSupported     = $ByteReader.ReadBool() # 00000000 00001000
    DeviceEncryptsAllUserData                   = $ByteReader.ReadBool() # 00000000 00010000
    ReadZeroAfterTrimSupported                  = $ByteReader.ReadBool() # 00000000 00100000
    Optional28BitCommandsSupported              = $ByteReader.ReadBool() # 00000000 01000000
    IEEE1667                                    = $ByteReader.ReadBool() # 00000000 10000000
    DownloadMicrocodeDmaSupported               = $ByteReader.ReadBool() # 00000001 00000000
    SetMaxSetPasswordUnlockDmaSupported         = $ByteReader.ReadBool() # 00000010 00000000
    WriteBufferDmaSupported                     = $ByteReader.ReadBool() # 00000100 00000000
    ReadBufferDmaSupported                      = $ByteReader.ReadBool() # 00001000 00000000
    DeviceConfigIdentifySetDmaSupported         = $ByteReader.ReadBool() # 00010000 00000000
    LPSAERCSupported                            = $ByteReader.ReadBool() # 00100000 00000000
    DeterministicReadAfterTrimSupported         = $ByteReader.ReadBool() # 01000000 00000000
    CFastSpecSupported                          = $ByteReader.ReadBool() # 10000000 00000000

    ReservedWords70                             = $ByteReader.GetWord(5) # Word 70-74

    # Word 75 - Queue depth
    QueueDepth                                  = $ByteReader.ReadInt(5) + 1 # 00000000 00011111
    QueueDepthReserved                          = $ByteReader.ReadInt(11)    # 11111111 11100000

    # Word 76 - Serial ATA Capabilities
    SataCapabilitiesReserved0                   = $ByteReader.ReadBool() # 00000000 00000001
    SataGen1                                    = $ByteReader.ReadBool() # 00000000 00000010
    SataGen2                                    = $ByteReader.ReadBool() # 00000000 00000100
    SataGen3                                    = $ByteReader.ReadBool() # 00000000 00001000
    SataCapabilitiesReserved1                   = $ByteReader.ReadInt(4) # 00000000 11110000
    NCQ                                         = $ByteReader.ReadBool() # 00000001 00000000
    HIPM                                        = $ByteReader.ReadBool() # 00000010 00000000
    PhyEvents                                   = $ByteReader.ReadBool() # 00000100 00000000
    NcqUnload                                   = $ByteReader.ReadBool() # 00001000 00000000
    NcqPriority                                 = $ByteReader.ReadBool() # 00010000 00000000
    HostAutoPS                                  = $ByteReader.ReadBool() # 00100000 00000000
    DeviceAutoPS                                = $ByteReader.ReadBool() # 01000000 00000000
    ReadLogDMA                                  = $ByteReader.ReadBool() # 10000000 00000000
    # Word 77 - Serial ATA Capabilities
    SataCapabilitiesReserved2                   = $ByteReader.ReadBool() # 00000000 00000001
    SataCapabilitiesCurrentSpeed                = $ByteReader.ReadInt(3) # 00000000 00001110
    NcqStreaming                                = $ByteReader.ReadBool() # 00000000 00010000
    NcqQueueMgmt                                = $ByteReader.ReadBool() # 00000000 00100000
    NcqReceiveSend                              = $ByteReader.ReadBool() # 00000000 01000000
    DEVSLPtoReducedPwrState                     = $ByteReader.ReadBool() # 00000000 10000000
    PowerDisableFeatureAlwaysEnabled            = $ByteReader.ReadBool() # 00000001 00000000
    OutOfBandManagementSupported                = $ByteReader.ReadBool() # 00000010 00000000
    SataCapabilitiesReserved3                   = $ByteReader.ReadInt(6) # 11111100 00000000

    # Word 78 - Serial ATA Features Supported
    SataSupportedMustBeFalse                    = $ByteReader.ReadBool() # 00000000 00000001
    SataSupportedNonZeroOffsets                 = $ByteReader.ReadBool() # 00000000 00000010
    SataSupportedDmaSetupAutoActivate           = $ByteReader.ReadBool() # 00000000 00000100
    SataSupportedDeviceInitiatedPowerManagement = $ByteReader.ReadBool() # 00000000 00001000
    SataSupportedInOrderDataDelivery            = $ByteReader.ReadBool() # 00000000 00010000
    SataSupportedHardwareFeatureControl         = $ByteReader.ReadBool() # 00000000 00100000
    SataSupportedSoftwareSettingsPreservation   = $ByteReader.ReadBool() # 00000000 01000000
    SataSupportedNCQAutosense                   = $ByteReader.ReadBool() # 00000000 10000000
    SataSupportedDeviceSleep                    = $ByteReader.ReadBool() # 00000001 00000000
    SataSupportedHybridInformation              = $ByteReader.ReadBool() # 00000010 00000000
    SataSupportedReserved1                      = $ByteReader.ReadBool() # 00000100 00000000
    SataSupportedRebuildAssist                  = $ByteReader.ReadBool() # 00001000 00000000
    SataSupportedPowerDisable                   = $ByteReader.ReadBool() # 00010000 00000000
    SataSupportedReserved2                      = $ByteReader.ReadInt(3) # 11100000 00000000

    # Word 79 - Serial ATA Features Enabled
    SataEnabledMustBeFalse                      = $ByteReader.ReadBool() # 00000000 00000001
    SataEnabledNonZeroOffsets                   = $ByteReader.ReadBool() # 00000000 00000010
    SataEnabledDmaSetupAutoActivate             = $ByteReader.ReadBool() # 00000000 00000100
    SataEnabledDeviceInitiatedPowerManagement   = $ByteReader.ReadBool() # 00000000 00001000
    SataEnabledInOrderDataDelivery              = $ByteReader.ReadBool() # 00000000 00010000
    SataEnabledHardwareFeatureControl           = $ByteReader.ReadBool() # 00000000 00100000
    SataEnabledSoftwareSettingsPreservation     = $ByteReader.ReadBool() # 00000000 01000000
    SataEnabledAutomaticPartialToSlumber        = $ByteReader.ReadBool() # 00000000 10000000
    SataEnabledDeviceSleep                      = $ByteReader.ReadBool() # 00000001 00000000
    SataEnabledHybridInformation                = $ByteReader.ReadBool() # 00000010 00000000
    SataEnabledPowerDisable                     = $ByteReader.ReadBool() # 00000100 00000000
    SataEnabledRebuildAssist                    = $ByteReader.ReadBool() # 00001000 00000000
    SataEnabledReserved1                        = $ByteReader.ReadInt(4) # 11110000 00000000

    # Word 80 - Major version number
    MajorRevisionReserved0                      = $ByteReader.ReadBool() # 00000000 00000001
    Ata1Supported                               = $ByteReader.ReadBool() # 00000000 00000010
    Ata2Supported                               = $ByteReader.ReadBool() # 00000000 00000100
    Ata3Supported                               = $ByteReader.ReadBool() # 00000000 00001000
    Ata4Supported                               = $ByteReader.ReadBool() # 00000000 00010000
    Ata5Supported                               = $ByteReader.ReadBool() # 00000000 00100000
    Ata6Supported                               = $ByteReader.ReadBool() # 00000000 01000000
    Ata7Supported                               = $ByteReader.ReadBool() # 00000000 10000000
    Ata8Supported                               = $ByteReader.ReadBool() # 00000001 00000000
    Acs2Supported                               = $ByteReader.ReadBool() # 00000010 00000000
    Acs3Supported                               = $ByteReader.ReadBool() # 00000100 00000000
    Acs4Supported                               = $ByteReader.ReadBool() # 00001000 00000000
    Acs5Supported                               = $ByteReader.ReadBool() # 00010000 00000000
    MajorRevisionReserved1                      = $ByteReader.ReadInt(3) # 11100000 00000000

    # Word 81 - Minor version number
    MinorRevision                               = $ByteReader.GetWord()

    # Word 82 - Command Set Support
    CommandSetSupportedSmartCommands            = $ByteReader.ReadBool() # 00000000 00000001
    CommandSetSupportedSecurityMode             = $ByteReader.ReadBool() # 00000000 00000010
    CommandSetSupportedRemovableMediaFeature    = $ByteReader.ReadBool() # 00000000 00000100
    CommandSetSupportedPowerManagement          = $ByteReader.ReadBool() # 00000000 00001000
    CommandSetSupportedReserved1                = $ByteReader.ReadBool() # 00000000 00010000
    CommandSetSupportedWriteCache               = $ByteReader.ReadBool() # 00000000 00100000
    CommandSetSupportedLookAhead                = $ByteReader.ReadBool() # 00000000 01000000
    CommandSetSupportedReleaseInterrupt         = $ByteReader.ReadBool() # 00000000 10000000
    CommandSetSupportedServiceInterrupt         = $ByteReader.ReadBool() # 00000001 00000000
    CommandSetSupportedDeviceReset              = $ByteReader.ReadBool() # 00000010 00000000
    CommandSetSupportedHostProtectedArea        = $ByteReader.ReadBool() # 00000100 00000000
    CommandSetSupportedObsolete1                = $ByteReader.ReadBool() # 00001000 00000000
    CommandSetSupportedWriteBuffer              = $ByteReader.ReadBool() # 00010000 00000000
    CommandSetSupportedReadBuffer               = $ByteReader.ReadBool() # 00100000 00000000
    CommandSetSupportedNop                      = $ByteReader.ReadBool() # 01000000 00000000
    CommandSetSupportedObsolete2                = $ByteReader.ReadBool() # 10000000 00000000
    # Word 83 - Command Set Support
    CommandSetSupportedDownloadMicrocode        = $ByteReader.ReadBool() # 00000000 00000001
    CommandSetSupportedDmaQueued                = $ByteReader.ReadBool() # 00000000 00000010
    CommandSetSupportedCfa                      = $ByteReader.ReadBool() # 00000000 00000100
    CommandSetSupportedAdvancedPm               = $ByteReader.ReadBool() # 00000000 00001000
    CommandSetSupportedMsn                      = $ByteReader.ReadBool() # 00000000 00010000
    CommandSetSupportedPowerUpInStandby         = $ByteReader.ReadBool() # 00000000 00100000
    CommandSetSupportedManualPowerUp            = $ByteReader.ReadBool() # 00000000 01000000
    CommandSetSupportedReserved2                = $ByteReader.ReadBool() # 00000000 10000000
    CommandSetSupportedSetMax                   = $ByteReader.ReadBool() # 00000001 00000000
    CommandSetSupportedAcoustics                = $ByteReader.ReadBool() # 00000010 00000000
    CommandSetSupportedBigLba                   = $ByteReader.ReadBool() # 00000100 00000000
    CommandSetSupportedDeviceConfigOverlay      = $ByteReader.ReadBool() # 00001000 00000000
    CommandSetSupportedFlushCache               = $ByteReader.ReadBool() # 00010000 00000000
    CommandSetSupportedFlushCacheExt            = $ByteReader.ReadBool() # 00100000 00000000
    CommandSetSupportedWordValid83              = $ByteReader.ReadInt(2) # 11000000 00000000
    # Word 84 - Command Set Support
    CommandSetSupportedSmartErrorLog            = $ByteReader.ReadBool() # 00000000 00000001
    CommandSetSupportedSmartSelfTest            = $ByteReader.ReadBool() # 00000000 00000010
    CommandSetSupportedMediaSerialNumber        = $ByteReader.ReadBool() # 00000000 00000100
    CommandSetSupportedMediaCardPassThrough     = $ByteReader.ReadBool() # 00000000 00001000
    CommandSetSupportedStreamingFeature         = $ByteReader.ReadBool() # 00000000 00010000
    CommandSetSupportedGpLogging                = $ByteReader.ReadBool() # 00000000 00100000
    CommandSetSupportedWriteFua                 = $ByteReader.ReadBool() # 00000000 01000000
    CommandSetSupportedWriteQueuedFua           = $ByteReader.ReadBool() # 00000000 10000000
    CommandSetSupportedWWN64Bit                 = $ByteReader.ReadBool() # 00000001 00000000
    CommandSetSupportedURGReadStream            = $ByteReader.ReadBool() # 00000010 00000000
    CommandSetSupportedURGWriteStream           = $ByteReader.ReadBool() # 00000100 00000000
    CommandSetSupportedReservedForTechReport    = $ByteReader.ReadInt(2) # 00011000 00000000
    CommandSetSupportedIdleWithUnloadFeature    = $ByteReader.ReadBool() # 00100000 00000000
    CommandSetSupportedWordValid                = $ByteReader.ReadInt(2) # 11000000 00000000

    # Word 85 - Command Set Active
    CommandSetActiveSmartCommands               = $ByteReader.ReadBool() # 00000000 00000001
    CommandSetActiveSecurityMode                = $ByteReader.ReadBool() # 00000000 00000010
    CommandSetActiveRemovableMediaFeature       = $ByteReader.ReadBool() # 00000000 00000100
    CommandSetActivePowerManagement             = $ByteReader.ReadBool() # 00000000 00001000
    CommandSetActiveReserved1                   = $ByteReader.ReadBool() # 00000000 00010000
    CommandSetActiveWriteCache                  = $ByteReader.ReadBool() # 00000000 00100000
    CommandSetActiveLookAhead                   = $ByteReader.ReadBool() # 00000000 01000000
    CommandSetActiveReleaseInterrupt            = $ByteReader.ReadBool() # 00000000 10000000
    CommandSetActiveServiceInterrupt            = $ByteReader.ReadBool() # 00000001 00000000
    CommandSetActiveDeviceReset                 = $ByteReader.ReadBool() # 00000010 00000000
    CommandSetActiveHostProtectedArea           = $ByteReader.ReadBool() # 00000100 00000000
    CommandSetActiveObsolete1                   = $ByteReader.ReadBool() # 00001000 00000000
    CommandSetActiveWriteBuffer                 = $ByteReader.ReadBool() # 00010000 00000000
    CommandSetActiveReadBuffer                  = $ByteReader.ReadBool() # 00100000 00000000
    CommandSetActiveNop                         = $ByteReader.ReadBool() # 01000000 00000000
    CommandSetActiveObsolete2                   = $ByteReader.ReadBool() # 10000000 00000000
    # Word 86 - Command Set Active
    CommandSetActiveDownloadMicrocode           = $ByteReader.ReadBool() # 00000000 00000001
    CommandSetActiveDmaQueued                   = $ByteReader.ReadBool() # 00000000 00000010
    CommandSetActiveCfa                         = $ByteReader.ReadBool() # 00000000 00000100
    CommandSetActiveAdvancedPm                  = $ByteReader.ReadBool() # 00000000 00001000
    CommandSetActiveMsn                         = $ByteReader.ReadBool() # 00000000 00010000
    CommandSetActivePowerUpInStandby            = $ByteReader.ReadBool() # 00000000 00100000
    CommandSetActiveManualPowerUp               = $ByteReader.ReadBool() # 00000000 01000000
    CommandSetActiveReserved2                   = $ByteReader.ReadBool() # 00000000 10000000
    CommandSetActiveSetMax                      = $ByteReader.ReadBool() # 00000001 00000000
    CommandSetActiveAcoustics                   = $ByteReader.ReadBool() # 00000010 00000000
    CommandSetActiveBigLba                      = $ByteReader.ReadBool() # 00000100 00000000
    CommandSetActiveDeviceConfigOverlay         = $ByteReader.ReadBool() # 00001000 00000000
    CommandSetActiveFlushCache                  = $ByteReader.ReadBool() # 00010000 00000000
    CommandSetActiveFlushCacheExt               = $ByteReader.ReadBool() # 00100000 00000000
    CommandSetActiveReserved3                   = $ByteReader.ReadBool() # 01000000 00000000
    CommandSetActiveWords119_120Valid           = $ByteReader.ReadBool() # 10000000 00000000
    # Word 87 - Command Set Active
    CommandSetActiveSmartErrorLog               = $ByteReader.ReadBool() # 00000000 00000001
    CommandSetActiveSmartSelfTest               = $ByteReader.ReadBool() # 00000000 00000010
    CommandSetActiveMediaSerialNumber           = $ByteReader.ReadBool() # 00000000 00000100
    CommandSetActiveMediaCardPassThrough        = $ByteReader.ReadBool() # 00000000 00001000
    CommandSetActiveStreamingFeature            = $ByteReader.ReadBool() # 00000000 00010000
    CommandSetActiveGpLogging                   = $ByteReader.ReadBool() # 00000000 00100000
    CommandSetActiveWriteFua                    = $ByteReader.ReadBool() # 00000000 01000000
    CommandSetActiveWriteQueuedFua              = $ByteReader.ReadBool() # 00000000 10000000
    CommandSetActiveWWN64Bit                    = $ByteReader.ReadBool() # 00000001 00000000
    CommandSetActiveURGReadStream               = $ByteReader.ReadBool() # 00000010 00000000
    CommandSetActiveURGWriteStream              = $ByteReader.ReadBool() # 00000100 00000000
    CommandSetActiveReservedForTechReport       = $ByteReader.ReadInt(2) # 00011000 00000000
    CommandSetActiveIdleWithUnloadFeature       = $ByteReader.ReadBool() # 00100000 00000000
    CommandSetActiveReserved4                   = $ByteReader.ReadInt(2) # 11000000 00000000

    # Word 88 - Ultra DMA modes
    UltraDMASupportMode0                        = $ByteReader.ReadBool() # 00000000 00000001
    UltraDMASupportMode1                        = $ByteReader.ReadBool() # 00000000 00000010
    UltraDMASupportMode2                        = $ByteReader.ReadBool() # 00000000 00000100
    UltraDMASupportMode3                        = $ByteReader.ReadBool() # 00000000 00001000
    UltraDMASupportMode4                        = $ByteReader.ReadBool() # 00000000 00010000
    UltraDMASupportMode5                        = $ByteReader.ReadBool() # 00000000 00100000
    UltraDMASupportMode6                        = $ByteReader.ReadBool() # 00000000 01000000
    UltraDMASupportReserved                     = $ByteReader.ReadBool() # 00000000 10000000
    UltraDMAActiveMode0                         = $ByteReader.ReadBool() # 00000001 00000000
    UltraDMAActiveMode1                         = $ByteReader.ReadBool() # 00000010 00000000
    UltraDMAActiveMode2                         = $ByteReader.ReadBool() # 00000100 00000000
    UltraDMAActiveMode3                         = $ByteReader.ReadBool() # 00001000 00000000
    UltraDMAActiveMode4                         = $ByteReader.ReadBool() # 00010000 00000000
    UltraDMAActiveMode5                         = $ByteReader.ReadBool() # 00100000 00000000
    UltraDMAActiveMode6                         = $ByteReader.ReadBool() # 01000000 00000000
    UltraDMAActiveReserved                      = $ByteReader.ReadBool() # 10000000 00000000

    # Word 89 - Normal Security Erase Unit
    NormalSecurityEraseUnitTimeRequired         = $ByteReader.ReadInt(15) # 01111111 11111111
    NormalSecurityEraseUnitExtTimeReported      = $ByteReader.ReadBool()  # 10000000 00000000

    # Word 90 - Enhanced Security EraseUnit
    EnhancedSecurityEraseUnitTimeRequired       = $ByteReader.ReadInt(15) # 01111111 11111111
    EnhancedSecurityEraseUnitExtTimeReported    = $ByteReader.ReadBool()  # 10000000 00000000

    # Word 91 - Current APM level
    CurrentAPMLevel                             = $ByteReader.ReadInt(8) # 00000000 11111111
    ReservedWord91                              = $ByteReader.ReadInt(8) # 11111111 00000000

    # Word 92 - Master Password Identifier
    MasterPasswordID                            = $ByteReader.GetWord()

    # Word 93 - Hardware Reset Result (PATA)
    HardwareResetResultPATA                     = $ByteReader.ReadBool() # 00000000 00000001
                                                                         # 00000000 00000110
    HardwareResetResultDetectionMethodDevice0   = switch ( $ByteReader.ReadInt(2) ) {
        0 { '-'; break }
        1 { 'Jumper'; break }
        2 { 'CSEL'; break }
        3 { 'Other'; break }
    }
    HardwareResetResultPassedDiagnosticsDevice0 = $ByteReader.ReadBool() # 00000000 00001000
    HardwareResetResultDetectedPDIAGDevice0     = $ByteReader.ReadBool() # 00000000 00010000
    HardwareResetResultDetectedDASPDevice0      = $ByteReader.ReadBool() # 00000000 00100000
    HardwareResetResultDev0RespondsDev1Selected = $ByteReader.ReadBool() # 00000000 01000000
    HardwareResetResultReserved0                = $ByteReader.ReadBool() # 00000000 10000000
    HardwareResetResultMustBeTrue               = $ByteReader.ReadBool() # 00000001 00000000
                                                                         # 00000110 00000000
    HardwareResetResultDetectionMethodDevice1   = switch ( $ByteReader.ReadInt(2) ) {
        0 { '-'; break }
        1 { 'Jumper'; break }
        2 { 'CSEL'; break }
        3 { 'Other'; break }
    }
    HardwareResetResultDetectedPDIAGDevice1     = $ByteReader.ReadBool() # 00001000 00000000
    HardwareResetResultReserved1                = $ByteReader.ReadBool() # 00010000 00000000
    HardwareResetResultCBLIDAboveViHB           = $ByteReader.ReadBool() # 00100000 00000000
    HardwareResetResultMustBe1                  = $ByteReader.ReadInt(2) # 11000000 00000000

    # Word 94 - AAM level
    CurrentAcousticValue                        = $ByteReader.ReadInt(8) # 00000000 11111111
    RecommendedAcousticValue                    = $ByteReader.ReadInt(8) # 11111111 00000000

    StreamMinRequestSize                        = $ByteReader.GetWord()  # Word 95
    StreamingTransferTimeDMA                    = $ByteReader.GetWord()  # Word 96
    StreamingAccessLatencyDMAPIO                = $ByteReader.GetWord()  # Word 97
    StreamingPerfGranularity                    = $ByteReader.GetDWord() # Word 98-99
    Max48BitLBA                                 = $ByteReader.GetQWord() # Word 100-103
    StreamingTransferTime                       = $ByteReader.GetWord()  # Word 104
    DsmCap                                      = $ByteReader.GetWord()  # Word 105

    # Word 106 - Physical / Logical Sector Size
    LogicalSectorsPerPhysicalSector             = [Math]::Pow(2, $ByteReader.ReadInt(4)) # 00000000 00001111
    PhysicalLogicalSectorSizeReserved0          = $ByteReader.ReadInt(8)                 # 00001111 11110000
    LogicalSectorLongerThan256Words             = $ByteReader.ReadBool()                 # 00010000 00000000
    MultipleLogicalSectorsPerPhysicalSector     = $ByteReader.ReadBool()                 # 00100000 00000000
    PhysicalLogicalSectorSizeReserved1          = $ByteReader.ReadInt(2)                 # 11000000 00000000

    InterSeekDelay                              = $ByteReader.GetWord()  # Word 107
    WorldWideName                               = $ByteReader.GetHex(8)  # Word 108-111
    ReservedForWorldWideName128                 = $ByteReader.GetHex(8)  # Word 112-115
    ReservedForTlcTechnicalReport               = $ByteReader.GetWord()  # Word 116
    WordsPerLogicalSector                       = $ByteReader.GetDWord() # Word 117-118

    # Word 119 - Command Set Support Extended
    CmdSetSupportExtReservedForDrqTechReport    = $ByteReader.ReadBool() # 00000000 00000001
    CmdSetSupportExtWriteReadVerify             = $ByteReader.ReadBool() # 00000000 00000010
    CmdSetSupportExtWriteUncorrectableExt       = $ByteReader.ReadBool() # 00000000 00000100
    CmdSetSupportExtReadWriteLogDmaExt          = $ByteReader.ReadBool() # 00000000 00001000
    CmdSetSupportExtDownloadMicrocodeMode3      = $ByteReader.ReadBool() # 00000000 00010000
    CmdSetSupportExtFreefallControl             = $ByteReader.ReadBool() # 00000000 00100000
    CmdSetSupportExtSenseDataReport             = $ByteReader.ReadBool() # 00000000 01000000
    CmdSetSupportExtExtendedPowerConditions     = $ByteReader.ReadBool() # 00000000 10000000
    CmdSetSupportExtAccessibleMaxAddressConfig  = $ByteReader.ReadBool() # 00000001 00000000
    CmdSetSupportExtDSN                         = $ByteReader.ReadBool() # 00000010 00000000
    CmdSetSupportExtReserved0                   = $ByteReader.ReadInt(4) # 00111100 00000000
    CmdSetSupportExtMustBe1                     = $ByteReader.ReadInt(2) # 11000000 00000000

    # Word 120 - Command Set Active Extended
    CmdSetActiveExtReservedForDrqTechReport     = $ByteReader.ReadBool() # 00000000 00000001
    CmdSetActiveExtWriteReadVerify              = $ByteReader.ReadBool() # 00000000 00000010
    CmdSetActiveExtWriteUncorrectableExt        = $ByteReader.ReadBool() # 00000000 00000100
    CmdSetActiveExtReadWriteLogDmaExt           = $ByteReader.ReadBool() # 00000000 00001000
    CmdSetActiveExtDownloadMicrocodeMode3       = $ByteReader.ReadBool() # 00000000 00010000
    CmdSetActiveExtFreefallControl              = $ByteReader.ReadBool() # 00000000 00100000
    CmdSetActiveExtSenseDataReport              = $ByteReader.ReadBool() # 00000000 01000000
    CmdSetActiveExtExtendedPowerConditions      = $ByteReader.ReadBool() # 00000000 10000000
    CmdSetActiveExtReserved0                    = $ByteReader.ReadBool() # 00000001 00000000
    CmdSetActiveExtDSN                          = $ByteReader.ReadBool() # 00000010 00000000
    CmdSetActiveExtReserved1                    = $ByteReader.ReadInt(4) # 00111100 00000000
    CmdSetActiveExtMustBe1                      = $ByteReader.ReadInt(2) # 11000000 00000000
    
    # Word 121-126
    ReservedForExpandedSupportAndActive         = $ByteReader.GetWord(6)

    # Word 127 - Removable Media Status Notification
    RemovableMediaStatusNotificationSupport     = $ByteReader.ReadBool()  # 00000000 00000001
    RemovableMediaStatusNotificationReserved    = $ByteReader.ReadInt(15) # 1111111 111111110

    # Word 128 - Security Status
    SecuritySupported                           = $ByteReader.ReadBool()  # 00000000 00000001
    SecurityEnabled                             = $ByteReader.ReadBool()  # 00000000 00000010
    SecurityLocked                              = $ByteReader.ReadBool()  # 00000000 00000100
    SecurityFrozen                              = $ByteReader.ReadBool()  # 00000000 00001000
    SecurityCountExpired                        = $ByteReader.ReadBool()  # 00000000 00010000
    EnhancedSecurityEraseSupported              = $ByteReader.ReadBool()  # 00000000 00100000
    SecurityStatusReserved0                     = $ByteReader.ReadInt(2)  # 00000000 11000000
                                                                          # 00000001 00000000
    MasterPasswordCapability                    = switch ( $ByteReader.ReadInt(1) ) {
        0 { 'High'; break }
        1 { 'Maximum'; break }
    }
    SecurityStatusReserved1                     = $ByteReader.ReadInt(7)  # 11111110 00000000

    # Word 129-159
    ReservedWord129                             = $ByteReader.GetWord(31)

    # Word 160 - CFA Power Mode 1
    CfaPowerMode1MaximumCurrentInMA             = $ByteReader.ReadInt(12) # 00001111 11111111
    CfaPowerMode1Disabled                       = $ByteReader.ReadBool()  # 00010000 00000000
    CfaPowerMode1Required                       = $ByteReader.ReadBool()  # 00100000 00000000
    CfaPowerMode1Reserved0                      = $ByteReader.ReadBool()  # 01000000 00000000
    CfaPowerMode1Word160Supported               = $ByteReader.ReadBool()  # 10000000 00000000

    # Word 161-167 - Reserved for CFA
    ReservedForCfaWord161                       = $ByteReader.GetWord(7)

    # Word 168 - Form Factor
                                                                          # 00000000 00001111
    NominalFormFactor                           = switch ( $ByteReader.ReadInt(4) ) {
        0 { '-'; break }
        1 { '5.25'; break }
        2 { '3.5'; break }
        3 { '2.5'; break }
        4 { '1.8'; break }
        5 { '<1.8'; break }
        6 { 'mSATA'; break }
        7 { 'M.2'; break }
        default { "Unknown: $_" }
    }
    ReservedWord168                             =  $ByteReader.ReadInt(12) # 11111111 11110000

    # Word 169 - DATA SET MANAGEMENT support
    SupportsTrim                                = $ByteReader.ReadBool()    # 00000000 00000001
    DataSetManagementFeatureReserved0           = $ByteReader.ReadInt(15)   # 11111111 11111110

    AdditionalProductID                         = $ByteReader.GetString(8)  # Word 170-173
    ReservedForCfaWord174                       = $ByteReader.GetWord(2)    # Word 174-175
    CurrentMediaSerialNumber                    = $ByteReader.GetString(60) # Word 176-205

    # Word 206 - SCT Command Transport
    SCTCmdTransSupported                        = $ByteReader.ReadBool() # 00000000 00000001
    SCTCmdTransReadWriteLongSupported           = $ByteReader.ReadBool() # 00000000 00000010
    SCTCmdTransWriteSameSupported               = $ByteReader.ReadBool() # 00000000 00000100
    SCTCmdTransErrorRecoveryControlSupported    = $ByteReader.ReadBool() # 00000000 00001000
    SCTCmdTransFeatureControlSupported          = $ByteReader.ReadBool() # 00000000 00010000
    SCTCmdTransDataTablesSupported              = $ByteReader.ReadBool() # 00000000 00100000
    SCTCmdTransReserved1                        = $ByteReader.ReadInt(6) # 00001111 11000000
    SCTCmdTransVendorSpecific                   = $ByteReader.ReadInt(4) # 11110000 00000000

    # Word 207-208
    ReservedWord207                             = $ByteReader.GetWord(2)

    # Word 209 - Block Alignment
    AlignmentOfLogicalWithinPhysical            = $ByteReader.ReadInt(14) # 00111111 11111111
    BlockAlignmentWord209Supported              = $ByteReader.ReadBool()  # 01000000 00000000
    BlockAlignmentReserved0                     = $ByteReader.ReadBool()  # 10000000 00000000

    WriteReadVerifySectorCountMode3Only         = $ByteReader.GetDWord()  # Word 210-211
    WriteReadVerifySectorCountMode2Only         = $ByteReader.GetDWord()  # Word 212-213

    # Word 214 - NV Cache Capabilities
    NVCachePowerModeSupported                   = $ByteReader.ReadBool() # 00000000 00000001
    NVCachePowerModeEnabled                     = $ByteReader.ReadBool() # 00000000 00000010
    NVCacheCapabilitiesReserved0                = $ByteReader.ReadInt(2) # 00000000 00001100
    NVCacheFeatureSetEnabled                    = $ByteReader.ReadBool() # 00000000 00010000
    NVCacheCapabilitiesReserved1                = $ByteReader.ReadInt(3) # 00000000 11100000
    NVCachePowerModeVersion                     = $ByteReader.ReadInt(4) # 00001111 00000000 
    NVCacheFeatureSetVersion                    = $ByteReader.ReadInt(4) # 11110000 00000000 

    #NVCacheSizeLSW                              = $ByteReader.GetWord()  # Word 215 - Microsoft
    #NVCacheSizeMSW                              = $ByteReader.GetWord()  # Word 216
    NVCacheSizeLogicalBlocks                    = $ByteReader.GetDWord() # Word 215-216 - smartmontools
    NominalMediaRotationRate                    = $ByteReader.GetWord()  # Word 217
    ReservedWord218                             = $ByteReader.GetWord()  # Word 218

    # Word 219 - NV Cache Options
    NVCacheEstimatedTimeToSpinUpInSeconds       = $ByteReader.ReadInt(8) # 00000000 11111111
    NVCacheOptionsReserved                      = $ByteReader.ReadInt(8) # 11111111 00000000

    # Word 220 - Write-Read-Verify Mode
    WriteReadVerifySectorCountMode              = $ByteReader.ReadInt(8) # 00000000 11111111
    ReservedWord220                             = $ByteReader.ReadInt(8) # 11111111 00000000

    # Word 221
    ReservedWord221                             = $ByteReader.GetWord()

    # Word 222 - Transport Major Version
    TransportMajorVersionATA8                   = $ByteReader.ReadBool() # 00000000 00000001
    'TransportMajorVersionATA7orSATA1.0a'       = $ByteReader.ReadBool() # 00000000 00000010
    'TransportMajorVersionSATA2.0'              = $ByteReader.ReadBool() # 00000000 00000100
    'TransportMajorVersionSATA2.5'              = $ByteReader.ReadBool() # 00000000 00001000
    'TransportMajorVersionSATA2.6'              = $ByteReader.ReadBool() # 00000000 00010000
    'TransportMajorVersionSATA3.0'              = $ByteReader.ReadBool() # 00000000 00100000
    'TransportMajorVersionSATA3.1'              = $ByteReader.ReadBool() # 00000000 01000000
    'TransportMajorVersionSATA3.2'              = $ByteReader.ReadBool() # 00000000 10000000
    'TransportMajorVersionSATA3.3'              = $ByteReader.ReadBool() # 00000001 00000000
    'TransportMajorVersionSATA3.4'              = $ByteReader.ReadBool() # 00000010 00000000
    'TransportMajorVersionSATA3.5'              = $ByteReader.ReadBool() # 00000100 00000000
    TransportMajorVersionReserved               = $ByteReader.ReadBool() # 00001000 00000000
                                                                         # 11110000 00000000
    TransportMajorVersionType                   = switch ( $ByteReader.ReadInt(4) ) {
        0x0 { 'Parallel'; break }
        0x1 { 'Serial'; break }
        0xe { 'PCIe'; break }
        default { "Unknown: $_" }
    }

    TransportMinorVersion                       = $ByteReader.GetWord()   # Word 223
    ReservedWord224                             = $ByteReader.GetWord(6)  # Word 224-229
    ExtendedNumberOfUserAddressableSectors      = $ByteReader.GetQWord()  # Word 230-233
    MinBlocksPerDownloadMicrocodeMode03         = $ByteReader.GetWord()   # Word 234
    MaxBlocksPerDownloadMicrocodeMode03         = $ByteReader.GetWord()   # Word 235
    ReservedWord236                             = $ByteReader.GetWord(19) # Word 236-254

    # Word 255 - Integrity
    Signature                                   = $ByteReader.GetHex(1)   # 00000000 11111111
    CheckSum                                    = $ByteReader.GetHex(1)   # 11111111 00000000
}