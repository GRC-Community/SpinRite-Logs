[CmdletBinding()]
param (
    [String]$RemoveFromFolder,
    [Switch]$WhatIf
)

Get-ChildItem -Path . -Recurse -File -Filter '*.LOG' | Where-Object 'Length' -gt 1500 | Group-Object 'Length' |
Where-Object 'Count' -gt 1 | ForEach-Object {

    Get-FileHash -Path $_.Group | Group-Object 'Hash' | Where-Object 'Count' -gt 1 | ForEach-Object {

        if ( $RemoveFromFolder ) {

            foreach ( $LogPath in $_.Group.Path ) {

                if ( $LogPath -like "*$RemoveFromFolder*" ) {

                    if ( $WhatIf ) {

                        $LogPath

                    } else {

                        Remove-Item $LogPath

                    }

                }

            }

        } else {
        
            $_.Name
            $_.Group.Path
            ''

        }

    }

}