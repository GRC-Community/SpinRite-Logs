[CmdletBinding()]
param ()

# Process every log that is less than 10 MiB.
$Logs = & '.\6.1\Import-SpinRite61Log.ps1' -LogPath '.\6.1\' -Verbose -Recurse -SkipSize 10mb

# Export everything to a CSV.
$Logs | Export-Csv -Path .\Logs.csv

# Find all logs that contain a complete run where Remaining Time is greater than 0.
# https://dev.grc.com/Steve/spinrite-v6.1/-/issues/252
$Logs | Where-Object {
    ($_.'remaining Time' -gt [TimeSpan]'00:00:00') -and
    ($_.'Stop Type' -eq 'completed') -and
    (100 -in $_.'Completed %', $_.'Location %', $_.'GSD Completed %')
} | Select-Object Owner, Log, Version, Level, Model, 'serial number', 'Remaining Time', 'Completed Time',
    'Completed %', 'Location %', 'GSD Completed %', 'Start %', 'End %', 'From %', 'To %' | Export-Csv -Path '.\Remaining Time.csv'

# Find all drive models that support Long Ops.
($Logs | Where-Object 'long ops' -eq 'YES' | Group-Object Model -NoElement).Name | Sort-Object | Out-File '.\Long Ops.txt'