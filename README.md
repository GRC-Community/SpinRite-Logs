# Adding your logs

## Important info

- **Please make sure to redact sensitive information like your SpinRite serial number!**
  - Thankfully, 6.1 logs don't include your SpinRite serial number.
- Please include the name you would like me to use for your folder.
- Please do not submit more than 1 batch of logs per day.

## Submission options

Here are a few options. I'm open to alternative methods as well :)

- ZIP them up and email them to contact-project+grc-community-spinrite-logs-42722961-issue-@incoming.gitlab.com
   - This will create a confidential issue. I will be able to see your email address. I will download the ZIP file, delete the issue, then add your logs.
- [Open an issue](https://gitlab.com/GRC-Community/SpinRite-Logs/-/issues/new) and attach your logs as a ZIP file.
   - Issues are public by default. You can mark it as confidential if you want to. I will download the ZIP file, delete the issue, then add your logs.
   - Alternatively, you can [open an issue](https://dev.grc.com/Steve/miscellany/-/issues/new) in the Miscellany project on dev.grc.com.
- [Fork this project](https://gitlab.com/GRC-Community/SpinRite-Logs/-/forks/new), add your logs, then [submit a merge request](https://gitlab.com/GRC-Community/SpinRite-Logs/-/merge_requests/new).

## Artifacts

CI/CD runs whenever logs are added. You can download the latest version of the files it outputs below:

- [Logs.csv](https://gitlab.com/api/v4/projects/42722961/jobs/artifacts/main/raw/Logs.csv?job=build-job)
  - This contains every property from every log.
- [Long Ops.txt](https://gitlab.com/api/v4/projects/42722961/jobs/artifacts/main/raw/Long%20Ops.txt?job=build-job)
  - Every Model of drive in this repository where `long ops` = `YES`.
- [Remaining Time.csv](https://gitlab.com/api/v4/projects/42722961/jobs/artifacts/main/raw/Remaining%20Time.csv?job=build-job)
  - All logs that contain a complete run where Remaining Time is greater than 0.
  - https://dev.grc.com/Steve/spinrite-v6.1/-/issues/252